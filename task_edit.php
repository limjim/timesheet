<?php
// $Header: /cvsroot/tsheet/timesheet.php/task_edit.php,v 1.6 2004/07/02 14:15:56 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//load local vars from superglobals
$task_id = $_REQUEST['task_id'];

//define the command menu
$commandMenu->add(new TextCommand("fa fa-backward", "Back", true, "javascript:history.back()"));

//query database for existing task values
list($qh, $num) = dbQuery("select task_id, proj_id, name, description, status from $TASK_TABLE where task_id = $task_id ");
$data = dbResult($qh);

list($qh, $num) = dbQuery("SELECT username from $TASK_ASSIGNMENTS_TABLE where proj_id = $data[proj_id] AND task_id = $task_id");
$selected_array = array();
$i = 0;
while ($datanext = dbResult($qh)) {
    $selected_array[$i] = $datanext["username"];
    $i++;
}
?> 
<html>
    <head>
        <title>Edit Task</title>
        <?php include ("header.inc"); ?>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 col-xs-12">
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit Task: <?php echo $data["name"]; ?></h3>
                                </div>

                                <form class="form-horizontal" action="task_action.php" method="post">
                                    <input type="hidden" name="action" value="edit">
                                    <input type="hidden" name="proj_id" value="<?php echo $data["proj_id"]; ?>">
                                    <input type="hidden" name="task_id" value="<?php echo $data["task_id"]; ?>">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="task_name" class="col-sm-3 control-label">Task Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control input-sm" id="task_name" name="name" placeholder="Task name" value="<?php echo $data["name"]; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description_task" class="col-sm-3 control-label">Description</label>
                                            <div class="col-sm-9">
                                                <textarea type="textarea" class="form-control" rows="4" wrap="virtual" id="description_task" name="description" placeholder="Description">
                                                    <?php
                                                    $data["description"] = stripslashes($data["description"]);
                                                    echo $data["description"];
                                                    ?>
                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Status:</label>
                                            <div class="col-sm-9">
                                                <?php proj_status_list("task_status", $data["status"]); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Assignments</label>
                                            <div class="col-sm-9">
                                                <?php multi_user_select_list("assigned[]", $selected_array); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <input type="submit" class="btn btn-info pull-right" value="Save">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </body>
</html>