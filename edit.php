<?php
// $Header: /cvsroot/tsheet/timesheet.php/edit.php,v 1.9 2005/02/03 08:06:10 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn()) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//load local vars from superglobals
$save_changes = isset($_REQUEST['save_changes']) ? $_REQUEST['save_changes'] : false;
$task_id = $_REQUEST['task_id'];
$proj_id = $_REQUEST['proj_id'];
$client_id = $_REQUEST['client_id'];
$trans_num = $_REQUEST['trans_num'];
$month = $_REQUEST['month'];
$day = $_REQUEST['day'];
$year = $_REQUEST['year'];
$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "edit";

if ($action == "saveChanges") {
    $clock_on_date_year = $_REQUEST['clock_on_date_year'];
    $clock_on_date_month = $_REQUEST['clock_on_date_month'];
    $clock_on_date_day = $_REQUEST['clock_on_date_day'];
    $clock_off_date_year = $_REQUEST['clock_off_date_year'];
    $clock_off_date_month = $_REQUEST['clock_off_date_month'];
    $clock_off_date_day = $_REQUEST['clock_off_date_day'];
    $clock_on_time_hour = $_REQUEST['clock_on_time_hour'];
    $clock_on_time_min = $_REQUEST['clock_on_time_min'];
    $clock_off_time_hour = $_REQUEST['clock_off_time_hour'];
    $clock_off_time_min = $_REQUEST['clock_off_time_min'];
    $log_message = $_REQUEST['log_message'];

    $clock_on_time_string = "$clock_on_date_year-$clock_on_date_month-$clock_on_date_day $clock_on_time_hour:$clock_on_time_min:00";
    $clock_off_time_string = "$clock_off_date_year-$clock_off_date_month-$clock_off_date_day $clock_off_time_hour:$clock_off_time_min:00";

    $queryString = "UPDATE $TIMES_TABLE SET start_time='$clock_on_time_string', " .
            "end_time='$clock_off_time_string', " .
            "log_message='$log_message', " .
            "task_id='$task_id', " .
            "proj_id='$proj_id' " .
            "WHERE " .
            "trans_num='$trans_num'";

    list($qh, $num) = dbQuery($queryString);

    Header("Location: daily.php?proj_id=$proj_id&task_id=$task_id&month=$month&year=$year&day=$day");
    exit;
}

//define the command menu
include("timesheet_menu.inc");

//get trans info
$trans_info = get_trans_info($trans_num);

//Due to a bug in mysql with converting to unix timestamp from the string, 
//we are going to use php's strtotime to make the timestamp from the string.
//the problem has something to do with timezones.
$trans_info["start_time"] = strtotime($trans_info["start_time_str"]);
$trans_info["end_time"] = strtotime($trans_info["end_time_str"]);

if ($action != "saveChanges") {
    $proj_id = $trans_info["proj_id"];
    $task_id = $trans_info["task_id"];
    $client_id = $trans_info["client_id"];
}

include("form_input.inc");
?>
<html>
    <head>
        <title>Edit Work Log Record for <? echo $contextUser; ?></title>
        <?php
        include("header.inc");
        include("client_proj_task_javascript.inc");
        ?>
    </head>
    <body class="skin-blue sidebar-mini" onload="doOnLoad();">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <form name="editForm" class='form-horizontal' action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" id="theForm">
                        <input type="hidden" name="year" value="<?php echo $year; ?>">
                        <input type="hidden" name="month" value="<?php echo $month; ?>">
                        <input type="hidden" name="day" value="<?php echo $day; ?>">
                        <input type="hidden" id="client_id" name="client_id" value="<?php echo $client_id; ?>">
                        <input type="hidden" id="proj_id" name="proj_id" value="<?php echo $proj_id; ?>">
                        <input type="hidden" id="task_id" name="task_id" value="<?php echo $task_id; ?>">																										
                        <input type="hidden" name="trans_num" value="<?php echo $trans_num; ?>">
                        <input type="hidden" name="action" value="saveChanges" />
                        <div class="box box-info">
                            <div class="box-header">
                                <h4> <a name="AddEdit">Edit Work Log Record:</a></h4>
                            </div>
                            <div class='box-body'>
                                <div class="row no-margin">
                                    <div class="col-md-offset-2 col-md-8 col-xs-12">
                                        <table width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label class="col-md-3">Client:</label>
                                                        <div class="col-md-9">
                                                            <select id="clientSelect" class="form-control input-sm" name="clientSelect" onChange="onChangeClientSelect();" style="width: 80%"/>
                                                        </div>
                                                    </div>
                                                </td>									
                                            </tr>																									
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label class="col-md-3">Project:</label>
                                                        <div class="col-md-9">
                                                            <select id="projectSelect" class="form-control input-sm" name="projectSelect" onChange="onChangeProjectSelect();" style="width: 80%"/>
                                                        </div>
                                                    </div>
                                                </td>									
                                            </tr>																		
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label class="col-md-3">Task:</label>
                                                        <div class="col-md-9">
                                                            <select id="taskSelect" class="form-control input-sm" name="taskSelect" onChange="onChangeTaskSelect();" style="width: 80%"/>
                                                        </div>
                                                    </div>
                                                </td>									
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class='row no-margin'>
                                    <div class="col-md-offset-2 col-md-8 col-xs-12">
                                        <div class='form-group'>
                                            <div class='col-md-3'>Start time:</div>
                                            <div class='col-md-2 no-padding'>
                                                <?php
                                                $hourInput = new HourInput("clock_on_time_hour");
                                                $hourInput->create(date("G", $trans_info["start_time"]));
                                                ?>
                                            </div>
                                            <div class='col-md-1 no-padding'>
                                                <?php
                                                $minuteInput = new MinuteInput("clock_on_time_min");
                                                $minuteInput->create(date("i", $trans_info["start_time"]));
                                                ?>

                                            </div>
                                            <div class='col-md-1'>on</div>
                                            <div class='col-md-2 no-padding'>
                                                <?php
                                                $monthInput = new MonthInput("clock_on_date_month");
                                                $monthInput->create(date("n", $trans_info["start_time"]));
                                                ?>
                                            </div>
                                            <div class='col-md-1 no-padding'>
                                                <?php
                                                $dayInput = new DayInput("clock_on_date_day");
                                                $dayInput->create(date("d", $trans_info["start_time"]));
                                                ?> 
                                            </div>
                                            <div class='col-md-1 no-padding'>
                                                <input type="text" class='form-control input-sm' name="clock_on_date_year" size="4" value="<?php echo date("Y", $trans_info["start_time"]); ?>">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class='row no-margin'>
                                    <div class="col-md-offset-2 col-md-8 col-xs-12">
                                        <div class='form-group'>
                                            <div class='col-md-3'>End time:</div>
                                            <div class='col-md-2 no-padding'>
                                                <?php
                                                $hourInput = new HourInput("clock_off_time_hour");
                                                $hourInput->create(date("G", $trans_info["end_time"]));
                                                ?>
                                            </div>
                                            <div class='col-md-1 no-padding'>
                                                <?php
                                                $minuteInput = new MinuteInput("clock_off_time_min");
                                                $minuteInput->create(date("i", $trans_info["end_time"]));
                                                ?>

                                            </div>
                                            <div class='col-md-1'>on</div>
                                            <div class='col-md-2 no-padding'>
                                                <?php
                                                $monthInput = new MonthInput("clock_off_date_month");
                                                $monthInput->create(date("n", $trans_info["end_time"]));
                                                ?>
                                            </div>
                                            <div class='col-md-1 no-padding'>
                                                <?php
                                                $dayInput = new DayInput("clock_off_date_day");
                                                $dayInput->create(date("d", $trans_info["end_time"]));
                                                ?>
                                            </div>
                                            <div class='col-md-1 no-padding'>
                                                <input type="text" class="form-control input-sm" name="clock_off_date_year" size="4" value="<?php echo date("Y", $trans_info["end_time"]); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class='row no-margin'>
                                    <div class="col-md-offset-2 col-md-8 col-xs-12">
                                        <div class='form-group'>
                                            <label>Log message</label>
                                        </div>
                                    </div>
                                </div>
                                <div class='row no-margin'>
                                    <div class="col-md-offset-2 col-md-8 col-xs-12">
                                        <div class='form-group'>
                                            <textarea name="log_message" class="form-control" rows="5"><?php echo trim(stripslashes($trans_info["log_message"])); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer text-center">
                                <div class='row no-margin'>
                                    <div class="col-md-offset-2 col-md-8 col-xs-12">
                                        <div class='form-group'>
                                            <input type="button" class="btn btn-info btn-sm" value="Save Changes" name="submitButton" id="submitButton" onClick="onSubmit();">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </form>
                </section>
            </div>
        </div>
    </body>
</html>
