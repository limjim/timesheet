<?php
$secParam = 4140;

$tmpHour = intval($secParam / 3600);
$tmpMin = intval(($secParam % 3600) / 60);
$tmpSec = ($secParam % 3600) % 60;

$hour = str_pad($tmpHour, 2, "0", STR_PAD_LEFT);
$min = str_pad($tmpMin, 2, "0", STR_PAD_LEFT);
$sec = str_pad($tmpSec, 2, "0", STR_PAD_LEFT);

if ($hour == 0) {
    $strTime = $min . ":" . $sec;
} 
else {
    $strTime = $hour . ":" . $min . ":" . $sec;
}

echo $strTime;
