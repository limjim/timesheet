<style>
    .back-g-total{
        background: #00a65a;
        color: #fff;
    }
    .back-g-total a{
        color: #fff;
    }
    .back-g-total-m{
        background: #00a65a;
        color: #fff;
    }
    .back-g-total-m a{
        color: #fff;
    }
    .font-light{
        font-size: 14px;
    }

    .fc-sun{
        color: red;
        min-height: 120px;
        min-width: 120px;
    }
    .fc-sun a{
        color: red;
    }

    .fc-custom a{
        color: #333;
    }

    .pro_title_font{
        font-size: 14px;
    }
    .table-custom tr td{
        border: 1px solid #ddd;
    }
    .table-custom tr th{
        border: 1px solid #ddd;
    }
    .table-custom{
        border: 1px solid #ddd;
    }
</style>
<?php
//$Header: /cvsroot/tsheet/timesheet.php/calendar.php,v 1.9 2005/05/23 05:39:38 vexil Exp $
if (!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn()) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);
$loggedInUser = strtolower($_SESSION['loggedInUser']);

if (empty($loggedInUser))
    errorPage("Could not determine the logged in user");

if (empty($contextUser))
    errorPage("Could not determine the context user");

//load local vars from superglobals
$year = isset($_REQUEST["year"]) ? $_REQUEST["year"] : (int) date("Y");
$month = isset($_REQUEST["month"]) ? $_REQUEST["month"] : (int) date("m");
$day = isset($_REQUEST["day"]) ? $_REQUEST["day"] : (int) date("j");
$proj_id = isset($_REQUEST["proj_id"]) ? $_REQUEST["proj_id"] : 0;
$task_id = isset($_REQUEST["task_id"]) ? $_REQUEST["task_id"] : 0;
$client_id = isset($_REQUEST["client_id"]) ? $_REQUEST["client_id"] : 0;

// Check project assignment.
if ($proj_id != 0) { // id 0 means 'All Projects'
    list($qh, $num) = dbQuery("select * from $ASSIGNMENTS_TABLE where proj_id='$proj_id' and username='$contextUser'");
    if ($num < 1)
        errorPage("You cannot access this project, because you are not assigned to it.");
} else
    $task_id = 0;


//a useful constant
define("A_DAY", 24 * 60 * 60);


//get the passed date (context date)
$todayDate = mktime(0, 0, 0, $month, $day, $year);
$todayYear = date("Y", $todayDate);
$todayMonth = date("n", $todayDate);
$todayDay = date("j", $todayDate);
$dateValues = getdate($todayDate);
$todayDayOfWeek = $dateValues["wday"];

//the day the week should start on: 0=Sunday, 1=Monday
$startDayOfWeek = getWeekStartDay();

//work out the start date by minusing enough seconds to make it the start day of week
$startDate = mktime(0, 0, 0, $month, 1, $year);
$startYear = date("Y", $startDate);
$startMonth = date("n", $startDate);
$startDay = date("j", $startDate);

// Calculate the previous month.
$last_month = $month - 1;
$last_year = $year;
if (!checkdate($last_month, 1, $last_year)) {
    $last_month += 12;
    $last_year --;
}

//calculate the next month
$next_month = $month + 1;
$next_year = $year;
if (!checkdate($next_month, 1, $next_year)) {
    $next_year++;
    $next_month -= 12;
}

//work out the end date by adding 7 days
$endDate = mktime(0, 0, 0, $next_month, 1, $next_year);
$endYear = date("Y", $endDate);
$endMonth = date("n", $endDate);
$endDay = date("n", $endDate);

// Get day of week of 1st of month
$dowForFirstOfMonth = date('w', mktime(0, 0, 0, $month, 1, $year));

//get the number of lead in days
$leadInDays = $dowForFirstOfMonth - $startDayOfWeek;
if ($leadInDays < 0)
    $leadInDays += 7;

//get the first printed date
$firstPrintedDate = $startDate - ($leadInDays * A_DAY);

////get todays values
//$today = time();
//$todayYear = date("Y", $today);
//$todayMonth = date("n", $today);
//$todayDay = date("j", $today);
//define the command menu
include("timesheet_menu.inc");

function print_totals($seconds, $type = "", $year, $month, $day) {

    //minus a week from the date given so we link to the start of that week
    $passedDate = mktime(0, 0, 0, $month, $day, $year);
    $passedDate -= A_DAY * 7;
    $year = date("Y", $passedDate);
    $month = date("n", $passedDate);
    $day = date("j", $passedDate);

    // Called from calendar.php to print out a line summing the hours worked in the past
    // week.  index.phtml must set all global variables.
    global $BREAK_RATIO, $client_id, $proj_id, $task_id;
//    print "</tr><tr>\n";
    if ($BREAK_RATIO > 0) {
        print "<td align=\"left\">";
        $break_sec = floor($BREAK_RATIO * $seconds);
        $seconds -= $break_sec;
        print "<font size=\"-1\">Break time: <font color=\"red\">" . formatSeconds($break_sec);
        print "</font></font></td><td align=\"right\">";
    } else {

        if ($type == "monthly") {
            print "<td class=\"back-g-total-m\" colspan=\"8\" align=\"right\">";
            print "<div class=\"row no-margin\" style=\"padding:10px;\">";
        } else {
            print "<td class=\"back-g-total\">";
            print "<div class=\"row no-margin\" style=\"padding:50 0 0 10;\">";
        }
    }

    if ($type == "monthly") {
        print "Monthly total: ";
        print "<span class=\"calendar_total_value_$type\">" . formatSeconds($seconds) . "</span></div></td>\n";
    } else {
        print "<a href=\"weekly.php?client_id=$client_id&proj_id=$proj_id&task_id=$task_id&year=$year&month=$month&day=$day\"><span class=\"calendar_total_value_$type\">" . formatSeconds($seconds) . "</span> </a>";
    }
}
?>
<html>
    <head>
        <title>Timesheet for <?php echo "$contextUser" ?></title>
        <?php
        include ("header.inc");
        ?>
    </head>
    <body class="skin-blue sidebar-mini" <?php
    if (isset($popup))
        echo "onLoad=window.open(\"popup.php?proj_id=$proj_id&task_id=$task_id\",\"Popup\",\"location=0,directories=no,status=no,menubar=no,resizable=1,width=420,height=205\");";
    ?>>
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" metho="get">
                        <input type="hidden" name="month" value=<?php echo $month; ?>>
                        <input type="hidden" name="year" value=<?php echo $year; ?>>
                        <input type="hidden" name="task_id" value=<?php echo $task_id; ?>>
                        <div class="box">
                            <div class="box-body">
                                <div class="row no-margin">
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Client:</label>
                                            <div class="col-sm-10">
                                                <?php client_select_list($client_id, $contextUser, false, false, true, false, "submit();"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Project:</label>
                                            <div class="col-sm-10">
                                                <?php project_select_list($client_id, false, $proj_id, $contextUser, false, true, "submit();"); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="box-body table-responsive no-padding ">
                                <div id="calendar" class="fc-ltr fc-unthemed">
                                    <div class="fc-toolbar">
                                        <div class="fc-left">
                                            <?php echo date('F Y', mktime(0, 0, 0, $month, 1, $year)); ?>
                                        </div>
                                        <div class="fc-right">
                                            <div class="btn-group">
                                                <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?client_id=<?php echo $client_id; ?>&proj_id=<?php echo $proj_id; ?>&task_id=<?php echo $task_id; ?>&year=<?php echo $last_year ?>&month=<?php echo $last_month ?>&day=<?php echo $todayDay; ?>" class="btn btn-sm btn-info">Prev</a>
                                                <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?client_id=<?php echo $client_id; ?>&proj_id=<?php echo $proj_id; ?>&task_id=<?php echo $task_id; ?>&year=<?php echo $next_year ?>&month=<?php echo $next_month ?>&day=<?php echo $todayDay; ?>" class="btn btn-sm btn-info">Next</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fc-view-container" style="margin-top: 35px;">
                                        <div class="fc-view fc-month-view fc-basic-view">
                                            <table class="table table-custom">
                                                <thead>
                                                    <tr>
                                                        <th class="fc-day-header fc-widget-header fc-mon">Mon</th>
                                                        <th class="fc-day-header fc-widget-header fc-tue">Tue</th>
                                                        <th class="fc-day-header fc-widget-header fc-wed">Wed</th>
                                                        <th class="fc-day-header fc-widget-header fc-thu">Thu</th>
                                                        <th class="fc-day-header fc-widget-header fc-fri">Fri</th>
                                                        <th class="fc-day-header fc-widget-header fc-sat">Sat</th>
                                                        <th class="fc-day-header fc-widget-header fc-sun">Sun</th>
                                                        <th width="9%" class="fc-day-header fc-widget-header fc-today">Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <?php {

                                                            //define the variable dayRow
                                                            $dayRow = 0;

                                                            // Print last months' days spots.
                                                            for ($i = 0; $i < $leadInDays; $i++) {
                                                                //while (($dayRow < $dowForFirstOfMonth) && ($dowForFirstOfMonth != 0)) {
                                                                ?>
                                                                <td class="fc-day fc-widget-content fc-sun fc-other-month fc-past">&nbsp;</td>
                                                                <?php
//                                                                print "<td class=\"fc-day fc-widget-content fc-sun fc-other-month fc-past\">&nbsp;</td>\n ";
                                                                $dayRow++;
                                                            }

                                                            // Get the Monthly data.
                                                            list($num, $qh) = get_time_date($month, $year, $contextUser, $proj_id, $client_id);


                                                            $i = 0;
                                                            $day = 1;
                                                            $tot_sec = 0;
                                                            $week_tot_sec = 0;
                                                            $day_tot_sec = 0;

                                                            $datatmp = array();
                                                            for ($i = 0; $i < $num; $i++) {
                                                                $data = dbResult($qh);
                                                                $datatmp [] = $data;
                                                            }

                                                            while (checkdate($month, $day, $year)) {
                                                                // Reset daily variables;
                                                                $day_tot_sec = 0;
                                                                $last_task_id = -1;
                                                                $last_proj_id = -1;
                                                                $last_client_id = -1;

                                                                // New Week.
                                                                if ((($dayRow % 7) == 0) && ($dowForFirstOfMonth != 0)) {

                                                                    print_totals($week_tot_sec, "weekly", $year, $month, $day);

                                                                    $week_tot_sec = 0;
                                                                    ?>
                                                                </tr>
                                                                <tr>
                                                                    <?php
                                                                } else
                                                                    $dowForFirstOfMonth = 1;

                                                                //define subtable		
                                                                if (($dayRow % 7) == 6) {
                                                                    ?>
                                                                    <td valign="top" class="fc-day fc-day-number fc-widget-content fc-thu fc-past fc-sun">
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                    <td valign="top" class="fc-day fc-day-number fc-widget-content fc-thu fc-past fc-custom">
                                                                        <div class="row no-margin" style="min-height: 120px; min-width: 120px;">
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <?php
                                                                        // Print out date.
                                                                        /* print "<tr><td valign=\"top\"><tt><A HREF=\"daily.php?month=$month&year=$year&".
                                                                          "day=$day&client_id=$client_id&proj_id=$proj_id&task_id=$task_id\">$day</a></tt></td></tr>"; */

                                                                        $popup_href = "javascript:void(0)\" onclick=window.open(\"popup.php" .
                                                                                "?client_id=$client_id" .
                                                                                "&proj_id=$proj_id" .
                                                                                "&task_id=$task_id" .
                                                                                "&year=$year" .
                                                                                "&month=$month" .
                                                                                "&day=$day" .
                                                                                "&destination=$_SERVER[PHP_SELF]" .
                                                                                "\",\"Popup\",\"location=0,directories=no,status=no,menubar=no,resizable=1,width=1024,height=420\") dummy=\"";
                                                                        ?>

                                                                        <div class="row no-margin">
                                                                            <div class="row no-margin">
                                                                                <a class="pull-right" href="daily.php?month=<?php echo $month; ?>&year=<?php echo $year; ?>&day=<?php echo $day; ?>&client_id=<?php echo $client_id; ?>&proj_id=<?php echo $proj_id; ?>&task_id=<?php $task_id; ?>"><?php echo $day ?></a>
                                                                                <a class="pull-left" href="<?php echo $popup_href ?>" class="action_link">
                                                                                    <i class="fa fa-plus"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>

                                                                        <?php
                                                                        $data_seen = 0;
                                                                        // If the day has data, print it.
                                                                        foreach ($datatmp as $key => $data) {

                                                                            //Due to a bug in mysql with converting to unix timestamp from the string, 
                                                                            //we are going to use php's strtotime to make the timestamp from the string.
                                                                            //the problem has something to do with timezones.
                                                                            $data["start_time"] = strtotime($data["start_time_str"]);
                                                                            $data["end_time"] = isset($data["end_time_str"]) ? strtotime($data["end_time_str"]) : "";
                                                                            // $data["end_time"] = strtotime($data["end_time_str"]);
                                                                            // if (
                                                                            // 		(($data["start_time"] < mktime(0,0,0,$month,$day,$year)) 
                                                                            // 		&& ($data["end_time"] > mktime(23,59,59,$month,$day,$year))) ||
                                                                            // 		(($data["start_time"] >= mktime(0,0,0,$month,$day,$year)) 
                                                                            // 		&& ($data["start_time"] <= mktime(23,59,59,$month,$day,$year))) ||
                                                                            // 		(($data["end_time"] >= mktime(0,0,0,$month,$day,$year)) 
                                                                            // 		&& ($data["end_time"] <= mktime(23,59,59,$month,$day,$year)))
                                                                            // 	 )

                                                                            if ((intval($data[0]) == $day)) {
                                                                                // This day has data in it.  Therefore we want to print out a summary at the bottom of each day.
                                                                                $data_seen = 1;
                                                                                $todays_total_sec = 0;

                                                                                //print out client name if its a new client
                                                                                if ($client_id == 0 && $last_client_id != $data["client_id"]) {
                                                                                    $last_client_id = $data["client_id"];
                                                                                    $clientName = $data["organisation"];
                                                                                    ?>
                                                                                    <!--                                                                                <div class="row no-margin">
                                                                                    <?php // echo $clientName ?>
                                                                                                                                                                    </div>-->
                                                                                    <?php
                                                                                }

                                                                                //print out project name if its a new project
                                                                                if ($proj_id == 0 && $last_proj_id != $data["proj_id"]) {
                                                                                    $last_proj_id = $data["proj_id"];
                                                                                    $projectName = $data["title"];
                                                                                    ?>
                                                                                    <div class="pro_title_font row no-margin external-event bg-light-blue ui-draggable ui-draggable-handle"><?php echo $projectName ?></div>

                                                                                    <?php
                                                                                }

                                                                                // Print out task name if it's a new task
                                                                                if ($last_task_id != $data["task_id"]) {
                                                                                    $last_task_id = $data["task_id"];
                                                                                    $taskName = $data["name"];
                                                                                    ?>
                                                                                    <div class="font-light row no-margin"><i><?php echo $taskName ?></i></div>
                                                                                    <?php
                                                                                }

                                                                                if ($data["diff_sec"] > 0) {
                                                                                    //if both start and end time are not today
                                                                                    if ($data["start_time"] < mktime(0, 0, 0, $month, $day, $year) && $data["end_time"] > mktime(23, 59, 59, $month, $day, $year)) {
                                                                                        $today_diff_sec = 24 * 60 * 60; //all day - no one should work this hard!
                                                                                        ?>
                                                                                        <div class="row no-margin"><font style="font-size: 10px; font-weight: bold;">...-...</font></div>

                                                                                        <?php
                                                                                    }
                                                                                    //if end time is not today
                                                                                    elseif ($data["end_time"] > mktime(23, 59, 59, $month, $day, $year)) {
                                                                                        $today_diff_sec = mktime(0, 0, 0, $month, $day, $year) + 24 * 60 * 60 - $data["start_time"];
                                                                                        ?>
                                                                                        <div class="font-light row no-margin"><font style="font-size: 10px; font-weight: bold;"><?php echo $data["start"] ?>-...</font></div>
                                                                                        <?php
                                                                                    }
                                                                                    //elseif start time is not today
                                                                                    elseif ($data["start_time"] < mktime(0, 0, 0, $month, $day, $year)) {
                                                                                        $today_diff_sec = $data["end_time"] - mktime(0, 0, 0, $month, $day, $year);
                                                                                        ?>
                                                                                        <div class="font-light row no-margin"><font style="font-size: 10px; font-weight: bold;">...-<?php echo $data["endd"] ?></font></div>
                                                                                        <?php
                                                                                    } else {
                                                                                        $today_diff_sec = $data["diff_sec"];
                                                                                        $startTimeStr = $data["start"];
                                                                                        $endTimeStr = $data["endd"];
                                                                                        ?>
                                                                                        <div class="font-light row no-margin"><font style="font-size: 10px; font-weight: bold;"><?php echo $startTimeStr . ' - ' . $endTimeStr; ?></font></div>
                                                                                        <?php
                                                                                    }

                                                                                    $tot_sec += $today_diff_sec;
                                                                                    $week_tot_sec += $today_diff_sec;
                                                                                    $day_tot_sec += $today_diff_sec;
                                                                                } else {
                                                                                    $startTimeStr = $data["start"];
                                                                                    ?>
                                                                                    <div class="font-light row no-margin"><font style="font-size: 10px; font-weight: bold;"><?php echo $startTimeStr ?>-...</font></div>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                        }

                                                                        if ($data_seen == 1) {
                                                                            ?>
                                                                            <div class="row no-margin font-light"><font style="font-size: 11px; font-weight: bold;"><?php echo formatSeconds($day_tot_sec); ?> </font></div>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </td>
                                                                <?php
                                                                $day++;
                                                                $dayRow++;
                                                            }
                                                            // Print the rest of the calendar.
                                                            while (($dayRow % 7) != 0) {
                                                                if (($dayRow % 7) == 6) {
                                                                    ?>
                                                                    <td class="fc-day fc-widget-content fc-tue fc-other-month fc-future">&nbsp;</td>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <td class="fc-day fc-widget-content fc-thu fc-past">&nbsp;</td>
                                                                    <?php
                                                                }
                                                                $dayRow++;
                                                            }

                                                            print_totals($week_tot_sec, "weekly", $year, $month, $day);

                                                            $week_tot_sec = 0;
                                                            ?>
                                                        </tr>
                                                        <tr>
                                                            <?php
                                                            print_totals($tot_sec, "monthly", $year, $month, $day);
                                                        }
                                                        ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </body>
</html>
