<?php
// $Header: /cvsroot/tsheet/timesheet.php/user_maint.php,v 1.7 2005/02/03 09:15:44 vexil Exp $
if (!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();

//define the command menu
include("timesheet_menu.inc");
?>
<head><title>User Management Page</title>
    <?php
    include ("header.inc");
    ?>
    <script language="javascript">

        function deleteUser(uid, username)
        {
            //get confirmation
            if (confirm("Deleting user '" + username + "' will also remove all related project and task assignments."))
            {
                document.userForm.action.value = "delete";
                document.userForm.uid.value = uid;
                document.userForm.username.value = username;
                document.userForm.submit();
            }
        }

        function editUser(uid, firstName, lastName, username, emailAddress, phone, billRate, password, isAdministrator)
        {
            document.userForm.uid.value = uid;
            document.userForm.first_name.value = firstName;
            document.userForm.last_name.value = lastName;
            document.userForm.username.value = username;
            document.userForm.email_address.value = emailAddress;
            document.userForm.phone.value = phone
            document.userForm.bill_rate.value = billRate;
            document.userForm.password.value = password;
            document.userForm.checkAdmin.checked = isAdministrator;
            onCheckAdmin();
            document.location.href = "#AddEdit";
        }

        function addUser()
        {
            //validation
            if (document.userForm.username.value == "")
                alert("You must enter a username that the user will log on with.");
            else if (document.userForm.username.value == "admin")
                alert("Invalid data.");
            else if (document.userForm.password.value == "")
                alert("You must enter a password that the user will log on with.");
            else
            {
                document.userForm.action.value = "addupdate";
                document.userForm.submit();
            }
        }

        function onCheckAdmin() {
            document.userForm.isAdministrator.value =
                    document.userForm.checkAdmin.checked;
        }

    </script>
</HEAD>
<body class="skin-blue sidebar-mini">
    <div class="wrapper">
        <?php include ("header_body.inc"); ?>
        <?php include ("left_menu.inc"); ?> 
        <div class="content-wrapper">
            <section class="content-header">
                <h1>Employees/Contractors:</h1>
            </section>
            <section class="content">
                <form class="form-horizontal" action="user_action.php" name="userForm" method="post">
                    <input type="hidden" name="action" value="">
                    <input type="hidden" name="uid" value="">
                    <div class="box">
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <tr role="row">
                                        <td>#</td>
                                        <td>First Name</td>
                                        <td>Last Name</td>
                                        <td>Access</td>
                                        <td>Login Username</td>
                                        <td>Email Address</td>
                                        <td>Phone Number</td>
                                        <td>Bill Rate</td>
                                        <td><i>Actions</i></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    list($qh, $num) = dbQuery("select * from $USER_TABLE where username!='guest' order by last_name, first_name");
                                    $j = 0;
                                    while ($data = dbResult($qh)) {
                                        $firstNameField = empty($data["first_name"]) ? "&nbsp;" : $data["first_name"];
                                        $lastNameField = empty($data["last_name"]) ? "&nbsp;" : $data["last_name"];
                                        $usernameField = empty($data["username"]) ? "&nbsp;" : $data["username"];
                                        $emailAddressField = empty($data["email_address"]) ? "&nbsp;" : $data["email_address"];
                                        $phoneField = empty($data["phone"]) ? "&nbsp;" : $data["phone"];
                                        $billRateField = empty($data["bill_rate"]) ? "&nbsp;" : $data["bill_rate"];
                                        $isAdministrator = ($data["level"] >= 10);
                                        $j++;
                                        ?>
                                        <tr role="row">
                                            <td align="center"><?php echo $j; ?></td>
                                            <td><?php echo $firstNameField; ?></td>
                                            <td><?php echo $lastNameField; ?></td>
                                            <td>
                                                <?php
                                                if ($isAdministrator) {
                                                    echo '<span style="color:red;">Admin</span>';
                                                } else {
                                                    echo 'Basic';
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $usernameField; ?></td>
                                            <td><?php echo $emailAddressField; ?></td>
                                            <td><?php echo $phoneField; ?></td>
                                            <td align="right"><?php echo $billRateField; ?></td>
                                            <td align="center">
                                                <a class="btn btn-xs btn-warning" href="javascript:editUser('<?php echo $data["uid"] ?>', '<?php echo $data["first_name"] ?>', '<?php echo $data["last_name"] ?>', '<?php echo $data["username"] ?>', '<?php echo $data["email_address"] ?>', '<?php echo $data["phone"] ?>', '<?php echo $data["bill_rate"] ?>', '<?php echo $data["password"] ?>', '<?php echo $isAdministrator ?>')"><i class="fa fa-edit"></i> Edit</a>
                                                <a class="btn btn-xs btn-danger" href="javascript:deleteUser('<?php echo $data["uid"]; ?>', '<?php echo $data["username"]; ?>')"> <i class="fa fa-remove"></i>  Delete</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="box box-info">
                        <div class="box-header">
                            <h4> <a name="AddEdit">	Add/Update Employee/Contractor:</a></h4>
                        </div>
                        <div class="box-body">
                            <div class="row no-margin">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="first_name" class="col-sm-3 control-label">First name:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="first_name" class="form-control input-sm" id="first_name" placeholder="First name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name" class="col-sm-3 control-label">Last name:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="last_name" class="form-control input-sm" id="last_name" placeholder="Last name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="user_name" class="col-sm-3 control-label">Username:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="username" class="form-control input-sm" id="user_name" placeholder="User name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-sm-3 control-label">Password:</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control input-sm" id="password" name="password" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="email_address" class="col-sm-3 control-label">Email address:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-sm" id="email_address" name="email_address" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="col-sm-3 control-label">Phone:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="bill_rate" class="col-sm-3 control-label">Bill rate:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-sm" id="bill_rate" name="bill_rate" placeholder="Bill rate">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-1">
                                            <input type="checkbox" name="checkAdmin" id="checkAdmin" value="" onClick="onCheckAdmin();"></input>
                                            <input type="hidden" name="isAdministrator" id="isAdministrator" value="false" />		
                                        </div>
                                        <label for="checkAdmin" class="col-sm-8 text-left">This user is an administrator</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-center">
                            <input type="button" name="addupdate" value="Add/Update Employee/Contractor" onclick="javascript:addUser()" class="btn btn-sm btn-info">
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</body>
</HTML>
