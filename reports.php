<?php
// $Header: /cvsroot/tsheet/timesheet.php/reports.php,v 1.5 2005/03/02 22:22:38 stormer Exp $
if (!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//load local vars from superglobals
$uid = isset($_REQUEST['uid']) ? $_REQUEST['uid'] : $contextUser;

//define the command menu
include("timesheet_menu.inc");

// Set default months
setReportDate($year, $month, $day, $next_week, $prev_week, $next_month, $prev_month, $time, $time_middle_month);
?>
<html>
    <head><title>Timesheet.php Reports Page</title>
        <?php include ("header.inc"); ?>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <div class="box">
                        <div class="box-body">
                            <div class="row no-margin">
                                <div class="col-md-4 col-xs-12">
                                    Reports <?php echo date('F d, Y', $time) ?>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="row no-margin">
                                        <?php
                                        printPrevNext($time, $next_week, $prev_week, $next_month, $prev_month, $time_middle_month, $uid);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <thead>
                                    <tr role="row">
                                        <th with="50%">Report Description</th>
                                        <th with="50%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hours worked by a specific user</td>
                                        <td>
                                            <a href="admin_report_specific_user.php?month=<?php print $month; ?>&year=<?php print $year; ?>&mode=monthly">Generate monthly</a> /
                                            <a href="admin_report_specific_user.php?month=<?php print $month; ?>&year=<?php print $year; ?>&mode=weekly">Generate weekly</a>
                                        </td>
                                    <tr>
                                    <tr>
                                        <td>Hours worked on specific project</td>
                                        <td>
                                            <a href="admin_report_specific_project.php?month=<?php print $month; ?>&year=<?php print $year; ?>&mode=monthly">Generate monthly</a> /
                                            <a href="admin_report_specific_project.php?month=<?php print $month; ?>&year=<?php print $year; ?>&mode=weekly">Generate weekly</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Hours worked for a specific client</td>
                                        <td>
                                            <a href="admin_report_specific_client.php?month=<?php print $month; ?>&year=<?php print $year; ?>&mode=monthly">Generate monthly</a> /
                                            <a href="admin_report_specific_client.php?month=<?php print $month; ?>&year=<?php print $year; ?>&mode=weekly">Generate weekly</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Hours worked by all users on all projects</td>
                                        <td>
                                            <a href="admin_report_all.php?month=<?php print $month; ?>&year=<?php print $year; ?>&mode=monthly">Generate monthly</a>
                                        </td>
                                    </tr>	
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </body>
</HTML>
