<?php
// $Header: /cvsroot/tsheet/timesheet.php/daily.php,v 1.7 2005/05/10 11:42:53 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn()) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

if (empty($contextUser))
    errorPage("Could not determine the context user");

//define the command menu
include("timesheet_menu.inc");

//check that the client id is valid
if ($client_id == 0)
    $client_id = getFirstClient();

//check that project id is valid
if ($proj_id == 0)
    $task_id = 0;

//calculate tomorrow and yesterday for "prev" & "next" buttons
$yesterday = mktime(0, 0, 0, $month, $day, $year) - 24 * 60 * 60;
$tomorrow = mktime(0, 0, 0, $month, $day, $year) + 24 * 60 * 60;

function getDailyTimes($month, $day, $year, $id, $proj_id) {
    include("table_names.inc");
    list($qhq, $numq) = dbQuery("select timeformat from $CONFIG_TABLE where config_set_id = '1'");
    $configData = dbResult($qhq);

    $query = "select date_format(start_time,'%d') as day_of_month, trans_num, ";

    if ($configData["timeformat"] == "12")
        $query .= "date_format(end_time, '%l:%i%p') as endd, date_format(start_time, '%l:%i%p') as start, ";
    else
        $query .= "date_format(end_time, '%k:%i') as endd, date_format(start_time, '%k:%i') as start, ";
    $query .= "unix_timestamp(end_time) - unix_timestamp(start_time) as diff_sec, " .
            "unix_timestamp(start_time) as start_time, " .
            "unix_timestamp(end_time) as end_time, " .
            "end_time as end_time_str, " .
            "start_time as start_time_str, " .
            "$PROJECT_TABLE.title as project_title, " .
            "$TASK_TABLE.name, $TIMES_TABLE.proj_id, $TIMES_TABLE.task_id " .
            "FROM $TIMES_TABLE, $TASK_TABLE, $PROJECT_TABLE " .
            "WHERE $TASK_TABLE.proj_id=$PROJECT_TABLE.proj_id AND " .
            "uid='$id' AND ";

    $query .= "$TASK_TABLE.task_id = $TIMES_TABLE.task_id AND " .
            "((start_time >= '$year-$month-$day 00:00:00' AND start_time <= '$year-$month-$day 23:59:59') " .
            " OR (end_time >= '$year-$month-$day 00:00:00' AND end_time <= '$year-$month-$day 23:59:59') " .
            " OR (start_time < '$year-$month-$day 00:00:00' AND end_time > '$year-$month-$day 23:59:59')) " .
            " order by day_of_month, start_time";

    list($my_qh, $num) = dbQuery($query);
    return array($num, $my_qh);
}

//include date input classes
include "form_input.inc";

list($num, $qh) = getDailyTimes($month, $day, $year, $contextUser, $proj_id);
?>
<html>
    <head>
        <title>Update timesheet for <?php echo $contextUser; ?></title>
        <?php
        include("header.inc");
        include("client_proj_task_javascript.inc");
        ?>
        <script language="Javascript">

            function delete_entry(transNum) {
                if (confirm('Are you sure you want to delete this time entry?'))
                    location.href = 'delete.php?client_id=<?php echo $client_id; ?>&proj_id=<?php echo $proj_id; ?>&task_id=<?php echo $task_id; ?>&trans_num=' + transNum;
            }

        </script>
    </HEAD>
    <body class="skin-blue sidebar-mini" onload="doOnLoad();">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <div class="box">
                        <div class="box-body">
                            <div class="row no-margin">
                                <div class="col-md-6 col-xs-12">
                                    <?php echo strftime("%A %B %d, %Y", mktime(0, 0, 0, $month, $day, $year)); ?>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="btn-group pull-right">
                                        <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?month=<?php echo date("m", $yesterday); ?>&year=<?php echo date("Y", $yesterday); ?>&day=<?php echo date("d", $yesterday); ?>&proj_id=<?php echo $proj_id; ?>" class="btn btn-sm btn-info">Prev</a>
                                        <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?month=<?php echo date("m", $tomorrow); ?>&year=<?php echo date("Y", $tomorrow); ?>&day=<?php echo date("d", $tomorrow); ?>&proj_id=<?php echo $proj_id; ?>" class="btn btn-sm btn-info">Next</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body table-responsive no-padding" style="margin-top: 20px;">
                            <table class="table table-hover table-bordered" style="font-size: 14px;">
                                <thead>
                                    <tr>
                                        <th>Project</th>
                                        <th>Task</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Total</th>
                                        <th width="15%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($num == 0) {
                                        ?>
                                        <tr>
                                            <td><i>No hours recorded.</i></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <?php
                                    } else {
                                        $last_task_id = -1;
                                        $today_total_sec = 0;
                                        $total_diff_sec = 0;

                                        while ($data = dbResult($qh)) {
                                            //Due to an inconsistency with mysql and php with converting to unix timestamp from the string, 
                                            //we are going to use php's strtotime to make the timestamp from the string.
                                            //the problem has something to do with timezones.
                                            $data["start_time"] = strtotime($data["start_time_str"]);
                                            $data["end_time"] = strtotime($data["end_time_str"]);

                                            //get the project title and task name
                                            $projectTitle = stripslashes($data["project_title"]);
                                            $taskName = stripslashes($data["name"]);
                                            ?>
                                            <tr>
                                                <td>
                                                    <!--<a href="javascript:void(0)" onclick="javascript:window.open('proj_info.php?proj_id=<?php echo $data["proj_id"] ?>', 'Project Info', 'location=0,directories=no,status=no,scrollbar=yes,menubar=no,resizable=1,width=500,height=200')"><?php echo $projectTitle ?></a>-->
                                                    <a href="#" data-toggle="modal" data-target="#myModal_<?php echo $data['proj_id']; ?>"><?php echo $projectTitle; ?></a>
                                                    <div id="myModal_<?php echo $data['proj_id']; ?>" class="modal fade" role="dialog">
                                                        <div class="modal-dialog modal-default">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                                    <h4 class="modal-title">Project info</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php
                                                                    $proj_id = $data['proj_id'];
                                                                    $query_project = "select distinct title, description," .
                                                                            "DATE_FORMAT(start_date, '%M %d, %Y') as start_date," .
                                                                            "DATE_FORMAT(deadline, '%M %d, %Y') as deadline," .
                                                                            "proj_status, proj_leader " .
                                                                            "from $PROJECT_TABLE " .
                                                                            "where $PROJECT_TABLE.proj_id=$proj_id";

                                                                    $query_p = "select distinct $PROJECT_TABLE.title, $PROJECT_TABLE.proj_id, $PROJECT_TABLE.client_id, $CLIENT_TABLE.organisation, " .
                                                                            "$PROJECT_TABLE.description, DATE_FORMAT(start_date, '%M %d, %Y') as start_date, DATE_FORMAT(deadline, '%M %d, %Y') as deadline, " .
                                                                            "$PROJECT_TABLE.proj_status, http_link " .
                                                                            "from $PROJECT_TABLE, $CLIENT_TABLE, $USER_TABLE " .
                                                                            "where $PROJECT_TABLE.proj_id=$proj_id  ";

//set up query
                                                                    $query_p = "select distinct $PROJECT_TABLE.title, $PROJECT_TABLE.proj_id, $PROJECT_TABLE.client_id, " .
                                                                            "$CLIENT_TABLE.organisation, $PROJECT_TABLE.description, " .
                                                                            "DATE_FORMAT(start_date, '%M %d, %Y') as start_date, " .
                                                                            "DATE_FORMAT(deadline, '%M %d, %Y') as deadline, " .
                                                                            "$PROJECT_TABLE.proj_status, http_link, proj_leader " .
                                                                            "FROM $PROJECT_TABLE, $CLIENT_TABLE, $USER_TABLE " .
                                                                            "WHERE $PROJECT_TABLE.proj_id=$proj_id AND " .
                                                                            "$CLIENT_TABLE.client_id=$PROJECT_TABLE.client_id " .
                                                                            "ORDER BY $PROJECT_TABLE.proj_id";

                                                                    list($qh_p, $num_p) = dbQuery($query_p);
                                                                    if ($num_p > 0) {

                                                                        //get the current record
                                                                        $data_p = dbResult($qh_p);

                                                                        //strip slashes
                                                                        $data_p["title"] = stripslashes($data_p["title"]);
                                                                        $data_p["organisation"] = stripslashes($data_p["organisation"]);
                                                                        $data_p["description"] = stripslashes($data_p["description"]);

                                                                        list($billqh, $bill_num) = dbquery("select sum(unix_timestamp(end_time) - unix_timestamp(start_time)) as total_time, " .
                                                                                "sum(bill_rate * ((unix_timestamp(end_time) - unix_timestamp(start_time))/(60*60))) as billed " .
                                                                                "from $TIMES_TABLE, $USER_TABLE " .
                                                                                "where end_time > 0 AND $TIMES_TABLE.proj_id = $data_p[proj_id] AND $USER_TABLE.username = $TIMES_TABLE.uid ");
                                                                        $bill_data = dbResult($billqh);
                                                                        ?>
                                                                        <div class="row no-margin">
                                                                            <div class="col-xs-12">
                                                                                <h4 class="box-title">
                                                                                    <?php
                                                                                    if ($data_p["http_link"] != "") {
                                                                                        ?>
                                                                                        <a href="<?php echo $data_p['http_link'] ?>"><span><?php echo $data_p['title'] ?></span></a>
                                                                                        <?php
                                                                                    } else {
                                                                                        ?>
                                                                                        <span><?php echo $data_p['title'] ?></span>
                                                                                        <?php
                                                                                    }
                                                                                    print "&nbsp;&nbsp;<span>&lt;$data_p[proj_status]&gt;</span>"
                                                                                    ?>
                                                                                </h4>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row no-margin">
                                                                            <div class="col-xs-12">
                                                                                <label>Description:</label><?php echo $data_p["description"]; ?>
                                                                            </div>

                                                                        </div>
                                                                        <div class="row no-margin">
                                                                            <div class="col-xs-12">
                                                                                <label>Client:</label> <?php echo $data_p["organisation"]; ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row no-margin">
                                                                            <div class="col-xs-6">
                                                                                <label>Total time:</label> <?php echo (isset($bill_data["total_time"]) ? formatSeconds($bill_data["total_time"]) : "0h 0m"); ?>
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <?php if ($authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) { ?>
                                                                                    <label>Total bill:</label> <b>$<?php echo (isset($bill_data["billed"]) ? $bill_data["billed"] : "0.00"); ?></b>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row no-margin">
                                                                            <div class="col-xs-12">
                                                                                <div class="row no-margin"><label>Project Leader:</label><?php echo $data_p['proj_leader'] ?> </div>
                                                                                <?php
                                                                                //display assigned users      
                                                                                list($qh2, $num_workers) = dbQuery("select distinct username from $ASSIGNMENTS_TABLE where proj_id = $data_p[proj_id]");
                                                                                if ($num_workers == 0) {
                                                                                    ?>
                                                                                    <font size=\"-1\">Nobody assigned to this project</font>
                                                                                    <?php
                                                                                } else {
                                                                                    $workers = '';
                                                                                    ?>
                                                                                    <div class="row no-margin">
                                                                                        <label> Assigned Users: </label>

                                                                                        <?php
                                                                                        for ($k = 0; $k < $num_workers; $k++) {
                                                                                            $worker = dbResult($qh2);
                                                                                            $workers .= "$worker[username], ";
                                                                                        }

                                                                                        //$workers = ereg_replace(", $", "", $workers);
                                                                                        ?>

                                                                                        <?php echo $workers; ?>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row no-margin">

                                                                            <?php
                                                                            if (isset($data_p["start_date"]) && $data_p["start_date"] != '' && $data_p["deadline"] != '') {
                                                                                ?>
                                                                                <div class="col-xs-6">

                                                                                    <label>Start:</label> 
                                                                                    <?php echo date('d-m-Y', strtotime($data_p['start_date'])); ?>
                                                                                </div>
                                                                                <div class="col-xs-6">
                                                                                    <label>Deadline:</label> 
                                                                                    <?php echo date('d-m-Y', strtotime($data_p['deadline'])); ?>
                                                                                </div>
                                                                                <?php
                                                                            }
                                                                            ?>		

                                                                        </div>
                                                                        <?php if ($authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) { ?>
                                                                            <div class="no-margin row">
                                                                                <div class="col-xs-12"
                                                                                     <div class="project_task_list">
                                                                                        <a href="task_maint.php?proj_id=<?php echo $data["proj_id"]; ?>"><span>Tasks</span></a>
                                                                                        <?php
//                                                                                    //get tasks
//                                                                                    list($qh3, $num_tasks) = dbQuery("select name, task_id FROM $TASK_TABLE WHERE proj_id=$data[proj_id]");
//
//                                                                                    //are there any tasks?
//                                                                                    if ($num_tasks > 0) {
//                                                                                        while ($task_data = dbResult($qh3)) {
//                                                                                            $taskName = str_replace(" ", "&nbsp;", $task_data["name"]);
//                                                                                            print "<a href=\"javascript:void(0)\" onclick=window.open(\"task_info.php?proj_id=$data[proj_id]&task_id=$task_data[task_id]\",\"TaskInfo\",\"location=0,directories=no,status=no,menubar=no,resizable=1,width=550,height=220\")>$taskName</a><br>";
//                                                                                        }
//                                                                                    } else
//                                                                                        print "None.";
                                                                                        ?>	
                                                                                    </div>                                                                                                                                                                                                                                                         
                                                                                </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                </td>
                                                <td>
                                                    <!--<a href="javascript:void(0)" onclick="javascript:window.open('task_info.php?task_id=<?php echo $data["task_id"] ?>', 'Task Info', 'location=0,directories=no,status=no,scrollbar=yes,menubar=no,resizable=1,width=300,height=150')"><?php echo $taskName ?></a>-->
                                                    <?php
                                                    $task_id = $data['task_id'];
                                                    $query_task = "select distinct task_id, name, description,status, " .
                                                            "DATE_FORMAT(assigned, '%M %d, %Y') as assigned," .
                                                            "DATE_FORMAT(started, '%M %d, %Y') as started," .
                                                            "DATE_FORMAT(suspended, '%M %d, %Y') as suspended," .
                                                            "DATE_FORMAT(completed, '%M %d, %Y') as completed " .
                                                            "from $TASK_TABLE " .
                                                            "where $TASK_TABLE.task_id=$task_id " .
                                                            "order by $TASK_TABLE.task_id";

//get the proj_id for this task
                                                    if (!isset($proj_id)) {
                                                        list($qh_m, $num_m) = $proj_id = dbQuery("SELECT proj_id FROM $TASK_TABLE where task_id='$task_id'");
                                                        $results_m = dbResult($qh);
                                                        $proj_id = $results["proj_id"];
                                                    }

                                                    $query_project = "select distinct title, description," .
                                                            "DATE_FORMAT(start_date, '%M %d, %Y') as start_date," .
                                                            "DATE_FORMAT(deadline, '%M %d, %Y') as deadline," .
                                                            "proj_status, proj_leader " .
                                                            "from $PROJECT_TABLE " .
                                                            "where $PROJECT_TABLE.proj_id=$proj_id";
                                                    ?>
                                                    <div class="row no-margin">
                                                        <a href="#" data-toggle="modal" data-target="#myModal_t_<?php echo $data['task_id'] ?>"><?php echo $taskName; ?></a>
                                                        <div id="myModal_t_<?php echo $data['task_id']; ?>" class="modal fade" role="dialog">
                                                            <div class="modal-dialog modal-default">


                                                                <div class="modal-content text-left">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                                        <h4 class="modal-title">Task info</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <?php
                                                                        list($qh_m, $num_m) = dbQuery($query_task);
                                                                        if ($num_m > 0) {
                                                                            $data_task_ = dbResult($qh_m);
                                                                            ?>
                                                                            <div class="row no-margin">
                                                                                <div class="col-sm-10 col-xs-12">
                                                                                    <h4 class="box-title text-blue"><?php echo stripslashes($data_task_["name"]); ?></h4>
                                                                                </div>

                                                                                <div class="col-sm-2 col-xs-12" style="padding: 12px 0px 0px 0px;">
                                                                                    <small>&lt;<?php echo $data_task_["status"]; ?>&gt;</small>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row no-margin">
                                                                                <div class="col-sm-12 col-xs-12">
                                                                                    <label>Description:</label>
                                                                                    <?php echo stripslashes($data_task_["description"]); ?>

                                                                                </div>
                                                                            </div>
                                                                            <div class="row no-margin">
                                                                                <div class="col-sm-12 col-xs-12">
                                                                                    <label>Assigned persons:</label>
                                                                                    <?php
                                                                                    //get assigned users
                                                                                    list($qh3_, $num_3_) = dbQuery("select username, task_id from $TASK_ASSIGNMENTS_TABLE where task_id=$task_id");
                                                                                    if ($num_3_ > 0) {
                                                                                        while ($data_3_ = dbResult($qh3_)) {
                                                                                            print "$data_3_[username], ";
                                                                                        }
                                                                                    } else {
                                                                                        print "<i>None</i>";
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div>
                                                                        <?php }
                                                                        ?>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </td>
                                                <?php
                                                if ($data["diff_sec"] > 0) {
                                                    //if both start and end time are not today
                                                    if ($data["start_time"] < mktime(0, 0, 0, $month, $day, $year) &&
                                                            $data["end_time"] > mktime(23, 59, 59, $month, $day, $year)) {
                                                        $today_diff_sec = 24 * 60 * 60; //all day - no one should work this hard!

                                                        echo "<td class=\"calendar_cell_middle\" align=\"right\"><font color=\"#909090\"><i>" . $data["start"] . "," .
                                                        "<a href=\"daily.php?month=" . date("m", $data["start_time"]) .
                                                        "&year=" . date("Y", $data["start_time"]) .
                                                        "&day=" . date("d", $data["start_time"]) .
                                                        "&proj_id=$proj_id\"><i>" . date("d-M", $data["start_time"]) .
                                                        "</a></i></font>" .
                                                        "</td>" .
                                                        "<td class=\"calendar_cell_middle\" align=\"right\"><font color=\"#909090\"><i>" . $data["endd"] . "," .
                                                        "<a href=\"daily.php?month=" . date("m", $data["end_time"]) .
                                                        "&year=" . date("Y", $data["end_time"]) .
                                                        "&day=" . date("d", $data["end_time"]) .
                                                        "&proj_id=$proj_id\"><i>" . date("d-M", $data["end_time"]) .
                                                        "</a></i></font>" .
                                                        "</td>" .
                                                        "<td class=\"calendar_cell_middle\" align=\"right\">" . formatSeconds($today_diff_sec) . "<font color=\"#909090\"><i> of " .
                                                        formatSeconds($data["diff_sec"]) . "</i></font></TD>\n";
                                                    }
                                                    //if end time is not today
                                                    elseif ($data["end_time"] > mktime(23, 59, 59, $month, $day, $year)) {
                                                        $today_diff_sec = mktime(0, 0, 0, $month, $day, $year) + 24 * 60 * 60 - $data["start_time"];

                                                        echo "<td class=\"calendar_cell_middle\" align=\"right\">" . $data["start"] . "</td>" .
                                                        "<td class=\"calendar_cell_middle\" align=\"right\"><font color=\"#909090\"><i>" . $data["endd"] . "," .
                                                        "<a href=\"daily.php?month=" . date("m", $data["end_time"]) .
                                                        "&year=" . date("Y", $data["end_time"]) .
                                                        "&day=" . date("d", $data["end_time"]) .
                                                        "&proj_id=$proj_id\"><i>" . date("d-M", $data["end_time"]) .
                                                        "</a></i></font>" .
                                                        "</td>" .
                                                        "<td class=\"calendar_cell_middle\" align=\"right\">" . formatSeconds($today_diff_sec) . "<font color=\"#909090\"><i> of " . formatSeconds($data["diff_sec"]) . "</i></font></td>\n";
                                                    }
                                                    //elseif start time is not today
                                                    elseif ($data["start_time"] < mktime(0, 0, 0, $month, $day, $year)) {
                                                        $today_diff_sec = $data["end_time"] - mktime(0, 0, 0, $month, $day, $year);

                                                        echo "<td class=\"calendar_cell_middle\" align=\"right\"><font color=\"#909090\"><i>" . $data["start"] . "," .
                                                        "<a href=\"daily.php?month=" . date("m", $data["start_time"]) .
                                                        "&year=" . date("Y", $data["start_time"]) .
                                                        "&day=" . date("d", $data["start_time"]) .
                                                        "&proj_id=$proj_id\"><i>" . date("d-M", $data["start_time"]) .
                                                        "</a></i></font>" .
                                                        "</td>" .
                                                        "<td class=\"calendar_cell_middle\" align=\"right\">" . $data["endd"] . "</td>" .
                                                        "<td class=\"calendar_cell_middle\" align=\"right\">" . formatSeconds($today_diff_sec) . "<font color=\"#909090\"><i> of " .
                                                        formatSeconds($data["diff_sec"]) . "</i></font></td>\n";
                                                    } else {
                                                        $today_diff_sec = $data["diff_sec"];
                                                        ?>
                                                        <td  align="right"><?php echo $data["start"]; ?></td>
                                                        <td align="right"><?php echo $data["endd"] ?></td>
                                                        <td align="right"><?php echo formatSeconds($data["diff_sec"]) ?></td>
                                                        <?php
                                                    }
                                                    ?>
                                                    <td align="center">
                                                        <a class="btn btn-xs btn-info" href="edit.php?client_id=<?php echo $client_id ?>&proj_id=<?php echo $proj_id; ?>&task_id=<?php echo $task_id; ?>&trans_num=<?php echo $data["trans_num"] ?>&year=<?php echo $year ?>&month=<?php echo $month ?>&day=<?php echo $day; ?>"><i class="fa fa-eye"></i>  Details</a>
                                                        <!--<a href="delete.php?client_id=<?php echo $client_id ?>&proj_id=<?php echo $proj_id ?>&task_id=<?php echo $task_id ?>&trans_num=<?php echo $data["trans_num"] ?>">Delete</a>-->
                                                        <a class="btn btn-xs btn-warning" href="javascript:delete_entry(<?php echo $data["trans_num"] ?>);"><i class="fa fa-remove"></i>  Delete</a>
                                                    </td>
                                                    <?php
                                                    //add to todays total
                                                    $today_total_sec += $today_diff_sec;
                                                } else {
                                                    ?>
                                                    <td align="right"><?php echo $data["start"] ?></td>
                                                    <td align="right">&nbsp;</td>
                                                    <td  align="right">&nbsp;</td>
                                                    <td  align="center">
                                                        <a class="btn btn-xs btn-warning" href="javascript:delete_entry(<?php echo $data["trans_num"] ?>);"><i class="fa fa-remove"></i>  Delete</a>
                                                    </td>
                                                    <?php
                                                }
                                                ?>
                                            <tr>
                                                <?php
                                            }
                                            ?>
                                        <tr>
                                            <td colspan="5" align="right">
                                                Daily Total: <span > <?php formatSeconds($today_total_sec) ?> </span></td>
                                            <td  align="right">&nbsp;</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <form class="form-horizontal" action="action.php" method="post" name="addForm" id="theForm">
                        <input type="hidden" name="destination" value="daily">
                        <input type="hidden" name="year" value="<?php echo $year; ?>">
                        <input type="hidden" name="month" value="<?php echo $month; ?>">
                        <input type="hidden" name="day" value="<?php echo $day; ?>">
                        <input type="hidden" id="client_id" name="client_id" value="<?php echo $client_id; ?>">
                        <input type="hidden" id="proj_id" name="proj_id" value="<?php echo $proj_id; ?>">
                        <input type="hidden" id="task_id" name="task_id" value="<?php echo $task_id; ?>">																										
                        <input type="hidden" name="origin" value="<?php echo $_SERVER["PHP_SELF"]; ?>">
                        <input type="hidden" name="destination" value="<?php echo $_SERVER["PHP_SELF"]; ?>">
                        <div class="box box-info">
                            <div class="box-header">
                                <h4> <a name="AddEdit">Clock On / Off</a></h4>
                            </div>
                            <div class="box-body">
                                <div class="row no-margin">
                                    <div class="col-md-offset-2 col-md-8 col-xs-12">
                                        <table width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <div id="client_" class="hidden">
                                                        <div class="form-group">
                                                            <label class="col-md-3">Client:</label>
                                                            <div class="col-md-9">
                                                                <select id="clientSelect" class="form-control input-sm" name="clientSelect" onChange="onChangeClientSelect();" style="width: 80%"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>									
                                            </tr>																									
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label class="col-md-3">Project:</label>
                                                        <div class="col-md-9">
                                                            <select id="projectSelect" class="form-control input-sm" name="projectSelect" onChange="onChangeProjectSelect();" style="width: 80%"/>
                                                        </div>
                                                    </div>
                                                </td>									
                                            </tr>																		
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label class="col-md-3">Task:</label>
                                                        <div class="col-md-9">
                                                            <select id="taskSelect" class="form-control input-sm" name="taskSelect" onChange="onChangeTaskSelect();" style="width: 80%"/>
                                                        </div>
                                                    </div>
                                                </td>									
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <input type="checkbox" name="clock_on_check" id="clock_on_check" style="margin-right: 20px;" onclick="enableClockOn();">Clock on at

                                                        </div>
                                                        <div class="col-md-1 no-padding">
                                                            <?php
                                                            if (($year == date('Y')) && ($month == date('m')) && ($day == date('j'))):
                                                                ?>

                                                                <input type="radio" name="clock_on_radio" value="now" id="clock_on_radio_now" onclick="enableClockOn();">	now

                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-md-1 no-padding">
                                                            <?php
                                                            if (($year == date('Y')) && ($month == date('m')) && ($day == date('j'))):
                                                                ?>
                                                                <input type="radio" name="clock_on_radio" value="date" id="clock_on_radio_date" onclick="enableClockOn();" checked>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-md-2 no-padding">
                                                            <?php
                                                            $hourInput = new HourInput("clock_on_time_hour");
                                                            $hourInput->create(10);
                                                            ?>
                                                        </div>
                                                        <div class="col-md-2 no-padding">
                                                            <?php
                                                            $minuteInput = new MinuteInput("clock_on_time_min");
                                                            $minuteInput->create();
                                                            ?>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <input type="checkbox" name="clock_off_check" id="clock_off_check" style="margin-right: 20px;" onclick="enableClockOff();">Clock off at

                                                        </div>
                                                        <div class="col-md-1 no-padding">
                                                            <?php
                                                            if (($year == date('Y')) && ($month == date('m')) && ($day == date('j'))):
                                                                ?>

                                                                <input type="radio" name="clock_off_radio" id="clock_off_radio_now" value="now" onclick="enableClockOff();" checked>now

                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-md-1 no-padding">
                                                            <?php
// If the current day is today:
                                                            if (($year == date('Y')) && ($month == date('m')) && ($day == date('j'))):
                                                                ?>
                                                                <input type="radio" name="clock_off_radio" id="clock_off_radio_date" value="date" onclick="enableClockOff();">
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-md-2 no-padding">
                                                            <?php
                                                            $hourInput = new HourInput("clock_off_time_hour");
                                                            $hourInput->create(17);
                                                            ?>
                                                        </div>
                                                        <div class="col-md-2 no-padding">
                                                            <?php
                                                            $minuteInput = new MinuteInput("clock_off_time_min");
                                                            $minuteInput->create();
                                                            ?>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">
                                                    <div class="form-group">
                                                        <input type="button" class="btn btn-sm btn-info" value='Clock on and/or off' name="submitButton" id="submitButton" onClick="onSubmit();">
                                                    </div>
                                                </td>
                                            </tr>	

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </body>
</HTML>
