<?php
if (!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
// Authenticate
require("class.AuthenticationManager.php");

//check that this form has been submitted
if (isset($_POST["username"]) && isset($_POST["password"])) {
    //try logging the user in
    if (!$authenticationManager->login($_POST["username"], $_POST["password"]))
        $loginFailure = true;
    else {
        if (!empty($_REQUEST["redirect"]))
            header("Location: $_REQUEST[redirect]");
        else
            header("Location: calendar.php");

        exit();
    }
} else
//destroy the session by logging out
    $authenticationManager->logout();

function printMessage($message) {
    print "<div class=\"alert alert-danger display-hide\" style=\"display:block;\">
                    <button class=\"close\" data-close=\"alert\"></button>
                    <span>"
            . $message . "</span>
                </div>";
}

$redirect = isset($_REQUEST["redirect"]) ? $_REQUEST["redirect"] : "";
?>

<html>
    <head>
        <title>Timesheet Login</title>
        <?php
        include ("header.inc");
        ?>
    </head>
    <body onload="document.loginForm.username.focus();" class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="javascript:;"><b>NWS</b>TIMESHEET</a>
            </div>
            <div class="login-box-body">
                <?php
                if (isset($loginFailure))
                    printMessage($authenticationManager->getErrorMessage());
                else if (isset($_REQUEST["clearanceRequired"]))
                    printMessage("$_REQUEST[clearanceRequired] clearance is required for the page you have tried to access.");
                ?>
                <p class="login-box-msg">Sign in to start your session</p>
                <form class="login-form" action="login.php" method="POST" name="loginForm">
                    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"></input>
                    <div class="form-group has-feedback">
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                        </div>
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
