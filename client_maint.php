<?php
if (!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//make sure "No Client exists with client_id of 1
//execute the query	
tryDbQuery("INSERT INTO $CLIENT_TABLE VALUES (1,'No Client', 'This is required, do not edit or delete this client record', '', '', '', '', '', '', '', '', '', '', '', '', '', '');");
tryDbQuery("UPDATE $CLIENT_TABLE set organisation='No Client' WHERE client_id='1'");

//define the command menu
include("timesheet_menu.inc");
?>

<HTML>
    <HEAD>
        <TITLE>Client Management Page</TITLE>
        <?php
        include ("header.inc");
        ?>
        <script language="Javascript">

            function delete_client(clientId) {
                if (confirm('Are you sure you want to delete this client?'))
                    location.href = 'client_action.php?client_id=' + clientId + '&action=delete';
            }

        </script>
    </HEAD>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Clients:</h1>
                </section>
                <section class="content">
                    <form class="form-horizontal" action="client_action.php" method="post">
                        <div class="box">
                            <div class="box-header" style="margin-bottom: 10px;">
                                <div class="box-tools">
                                    <a class="btn btn-info btn-xs" href="client_add.php"><i class="fa fa-plus"></i>  Add new client</A>
                                </div>
                            </div>
                            <div class="box-body table-responsive no-padding">
                                <table id ="data_table" class="table table-bordered table-hover dataTable" role="g">
                                    <thead>
                                        <tr role="row">
                                            <th>Organisation</th>
                                            <th>Contact Name</th>
                                            <th>Phone</th>
                                            <th>Contact Email</th>
                                            <th width="20%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        list($qh, $num) = dbQuery("select * from $CLIENT_TABLE where client_id > 1 order by organisation");
                                        while ($data = dbResult($qh)) {
                                            $organisationField = stripslashes($data["organisation"]);
                                            if (empty($organisationField))
                                                $organisationField = "&nbsp;";
                                            $contactNameField = $data["contact_first_name"] . "&nbsp;" . $data["contact_last_name"];
                                            $phoneField = $data["phone_number"];
                                            if (empty($phoneField))
                                                $phoneField = "&nbsp;";
                                            $emailField = $data["contact_email"];
                                            if (empty($emailField))
                                                $emailField = "&nbsp;";
                                            ?>
                                            <tr role="row">
                                                <td>
                                                    <!--<a href="javascript:void(0)" onclick=window.open("client_info.php?client_id=<?php echo $data['client_id']; ?>","ClientInfo","location=0,directories=no,status=no,menubar=no,resizable=1,width=480,height=240")><?php echo $organisationField ?></a>-->
                                                    <a href="#" data-toggle="modal" data-target="#myModal_<?php echo $data['client_id']; ?>"><?php echo $organisationField; ?></a>
                                                    <div id="myModal_<?php echo $data['client_id']; ?>" class="modal fade" role="dialog">
                                                        <div class="modal-dialog modal-default">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                                    <h4 class="modal-title">Client info</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php
                                                                    $client_id = $data['client_id'];
                                                                    $query_cl = "select organisation, description, address1, address2," .
                                                                            "city, country, postal_code, contact_first_name, contact_last_name," .
                                                                            "username, contact_email, phone_number, fax_number, gsm_number, " .
                                                                            "http_url " .
                                                                            "from $CLIENT_TABLE " .
                                                                            "where $CLIENT_TABLE.client_id=$client_id";



                                                                    list($qh_cl, $num_cl) = dbQuery($query_cl);
                                                                    if ($num > 0) {

                                                                        $data_cl = dbResult($qh_cl);
                                                                        ?>
                                                                        <div class="row text-left">
                                                                            <div class="col-xs-12">
                                                                                <h3 class="box-title"><?php echo $data_cl['organisation'] ?></h3>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row text-left">
                                                                            <div class="col-sm-2">Description:</div>
                                                                            <label class="col-sm-10"><?php echo $data_cl['description']; ?></label>
                                                                        </div>
                                                                        <div class="row text-left">
                                                                            <div class="col-sm-2">Address1:</div>
                                                                            <label class="col-sm-10"><?php echo $data_cl['address1']; ?></label>
                                                                        </div>
                                                                        <div class="row text-left">
                                                                            <div class="col-sm-2">Address2:</div>
                                                                            <label class="col-sm-10"><?php echo $data_cl['address2'] ?></label>
                                                                        </div>
                                                                        <div class="row text-left">
                                                                            <div class="col-sm-2">ZIP, City:</div>
                                                                            <label class="col-sm-10 "><?php echo $data_cl['postal_code'];
                                                                echo $data_cl['city']; ?></label>
                                                                        </div>
                                                                        <div class="row text-left">
                                                                            <div class="col-sm-2">Country:</div>
                                                                            <label class="col-sm-10 "><?php echo $data_cl['country']; ?></label>
                                                                        </div>
                                                                        <div class="row text-left">
                                                                            <div class="col-sm-2">Contract:</div>
                                                                            <label class="col-sm-10 "><?php echo $data_cl['contact_first_name'];
                                                                echo $data_cl['contact_last_name']; ?></label>
                                                                        </div>
                                                                        <div class="row text-left">
                                                                            <div class="col-sm-2">Phone:</div>
                                                                            <label class="col-sm-10"><?php echo $data_cl['phone_number']; ?></label>
                                                                        </div>
                                                                        <div class="row text-left">
                                                                            <div class="col-sm-2">Fax:</div>
                                                                            <label class="col-sm-10"><?php echo $data_cl['fax_number']; ?></label>
                                                                        </div>
                                                                        <div class="row text-left">
                                                                            <div class="col-sm-2">Mobile:</div>
                                                                            <label class="col-sm-10"><?php echo $data_cl['gsm_number']; ?></label>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                </td>
                                                <td>
                                                    <?php echo $contactNameField; ?>
                                                </td>
                                                <td>
                                                    <?php echo $phoneField; ?>
                                                </td>
                                                <td>
    <?php echo $emailField; ?>
                                                </td>
                                                <td align="center">
                                                    <a class="btn btn-xs btn-warning" href='client_edit.php?client_id=<?php echo $data["client_id"] ?>'><i class='fa fa-edit'></i>  Edit</a>
                                                    <a class="btn btn-xs btn-danger" href='javascript:delete_client(<?php echo $data["client_id"] ?>);'><i class='fa fa-remove'></i>  Delete</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </body>    
</HTML>
