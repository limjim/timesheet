<header class="main-header">
    <a class="logo">
        <span class="logo-lg"><b>TimeSheet</b>NWS</span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li role="presentation" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i>  <span class="hidden-xs">Hello <?php parse_and_echo("%username%"); ?></span>
                    </a>
                    <ul class="dropdown-menu" >
                        <li><a href="changepwd.php" style="color: #777;"><i class="fa fa-refresh"></i>  Change Password</a></li>
                        <li class="divider"></li>
                        <li><a href="logout.php?logout=true" style="color: #777;"><i class="fa fa-sign-out"></i>  Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>



