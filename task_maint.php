<?php
if (!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
// $Header: /cvsroot/tsheet/timesheet.php/task_maint.php,v 1.11 2005/05/17 03:38:37 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//define the command menu
include("timesheet_menu.inc");

if (empty($proj_id))
    $proj_id = 1;

//make sure the selected project is valid for this client
if ($client_id != 0) {
    if (!isValidProjectForClient($proj_id, $client_id))
        $proj_id = getValidProjectForClient($client_id);
}

//set up the required queries
$query_task = "select distinct task_id, name, description,status, " .
        "DATE_FORMAT(assigned, '%M %d, %Y') as assigned," .
        "DATE_FORMAT(started, '%M %d, %Y') as started," .
        "DATE_FORMAT(suspended, '%M %d, %Y') as suspended," .
        "DATE_FORMAT(completed, '%M %d, %Y') as completed " .
        "from $TASK_TABLE " .
        "where $TASK_TABLE.proj_id=$proj_id " .
        "order by $TASK_TABLE.task_id";

$query_project = "select distinct title, description," .
        "DATE_FORMAT(start_date, '%M %d, %Y') as start_date," .
        "DATE_FORMAT(deadline, '%M %d, %Y') as deadline," .
        "proj_status, proj_leader " .
        "from $PROJECT_TABLE " .
        "where $PROJECT_TABLE.proj_id=$proj_id";
?>

<html>
    <head>
        <title>Tasks</title>
        <?php
        include ("header.inc");
        ?>
        <script language="Javascript">

            function delete_task(projectId, taskId) {
                if (confirm('Deleting a task which has been used in the past will make those timesheet ' +
                        'entries invalid, and may cause errors. This action is not recommended. ' +
                        'Are you sure you want to delete this task?'))
                    location.href = 'task_action.php?proj_id=' + projectId + '&task_id=' + taskId + '&action=delete';
            }

        </script>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <form name="changeForm" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
                        <div class="box box-group">
                            <div class="box-body">
                                <div class="row no-margin">
                                    <div class="col-md-5 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Client</label>
                                            <div class="col-sm-10">
                                                <?php client_select_list($client_id, 0, false, false, true, false, "submit();", false); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Project</label>
                                            <div class="col-sm-10">
                                                <?php project_select_list($client_id, false, $proj_id, 0, false, false, "submit();", false); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <?php if ($proj_id != 0) { ?>
                                                    <a class="btn btn-info btn-sm" href="task_add.php?proj_id=<?php echo $proj_id; ?>"><i class="fa fa-plus"></i>  Add new task</a>
                                                <?php } else { ?>
                                                    <span class="disabled"><i class="fa fa-plus"></i>  Add new task</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="box">
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th width="85%">
                                            Task
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    list($qh_task, $num_task) = dbQuery($query_task);
                                    if ($num_task == 0) {
                                        if ($proj_id == 0) {
                                            ?>
                                            <tr>
                                                <td align="left">
                                                    <i><br>Please select a client with projects, or 'All Clients'.<br><br></i>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <?php
                                        } else {
                                            ?>
                                            <tr>
                                                <td align="left">
                                                    <i><br>There are no tasks for this project.<br><br></i>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        for ($j = 0; $j < $num_task; $j++) {
                                            $data_task = dbResult($qh_task);
                                            //start the row
                                            ?>
                                            <tr>
                                                <td>
                                                    <div class="box" style="font-size: 14px; border-top: 1px solid #d2d6de; margin-bottom: 0px;">
                                                        <div class="box-header">
                                                            <h4 class="box-title text-green"><?php echo stripslashes($data_task["name"]); ?></h4>
                                                            &nbsp;<span class="text-light-blue">&lt;<?php echo $data_task["status"]; ?>&gt;</span><br>
                                                            <?php echo stripslashes($data_task["description"]); ?>  
                                                        </div>
                                                        <div class="box-header">
                                                            <h4 class="box-title text-green">Assigned persons:</h4><br>
                                                            <?php
                                                            list($qh3, $num_3) = dbQuery("select username, task_id from $TASK_ASSIGNMENTS_TABLE where task_id=$data_task[task_id]");
                                                            if ($num_3 > 0) {
                                                                while ($data_3 = dbResult($qh3)) {
                                                                    print "$data_3[username] ";
                                                                }
                                                            } else {
                                                                print "<i>None</i>";
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td align="center">
                                                    <a class="btn btn-xs btn-danger" href="task_edit.php?task_id=<?php echo $data_task["task_id"]; ?>"><i class="fa fa-edit"></i> Edit</a>  
                                                    <a class="btn btn-xs btn-warning" href="javascript:delete_task(<?php echo $proj_id; ?>,<?php echo $data_task["task_id"]; ?>);"><i class="fa fa-remove"></i> Delete</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </body>
</html>

