<style>
    .top10{margin-top: 10px;}
</style>
<?php
// $Header: /cvsroot/tsheet/timesheet.php/config.php,v 1.8 2005/02/03 08:06:10 vexil Exp $
if (!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//define the command menu
include("timesheet_menu.inc");

//Get the result set for the config set 1
list($qh, $num) = dbQuery("select locale, timezone, timeformat, headerhtml, bodyhtml, footerhtml, " .
        "errorhtml, bannerhtml, tablehtml, useLDAP, LDAPScheme, LDAPHost, LDAPPort, " .
        "LDAPBaseDN, LDAPUsernameAttribute, LDAPSearchScope, LDAPFilter, LDAPProtocolVersion, " .
        "LDAPBindUsername, LDAPBindPassword, weekstartday " .
        "from $CONFIG_TABLE where config_set_id = '1'");
$resultset = dbResult($qh);
?>
<html>
    <head>
        <title>Timesheet.php Configuration Parameters</title>
        <?php
        include ("header.inc");
        ?>
    </head>
    <script language="Javascript">

        //store the current LDAP entry method in this variable
        var currentLDAPEntryMethod = 'normal';

        function onChangeLDAPEntryMethod() {
            if (document.configurationForm.LDAPEntryMethod.value == 'normal') {
                document.getElementById('normalLDAPEntry').style.display = 'block';
                document.getElementById('advancedLDAPEntry').style.display = 'none';
            }
            else {
                document.getElementById('normalLDAPEntry').style.display = 'none';
                document.getElementById('advancedLDAPEntry').style.display = 'block';
            }

            //copy data from one to the other when it changes
            if (currentLDAPEntryMethod == 'normal' && document.configurationForm.LDAPEntryMethod.value != 'normal')
                buildLDAPUrlFromForm();
            else if (currentLDAPEntryMethod != 'normal' && document.configurationForm.LDAPEntryMethod.value == 'normal')
                fillOutLDAPFieldsFromUrl();

            //update the current LDAP entry method variable
            currentLDAPEntryMethod = document.configurationForm.LDAPEntryMethod.value;
        }

        function enableLDAP(value) {
            document.getElementById('LDAPEntryMethod').disabled = !value;
            document.getElementById('LDAPScheme').disabled = !value;
            document.getElementById('LDAPHost').disabled = !value;
            document.getElementById('LDAPPort').disabled = !value;
            document.getElementById('LDAPBaseDN').disabled = !value;
            document.getElementById('LDAPUsernameAttribute').disabled = !value;
            document.getElementById('LDAPSearchScope').disabled = !value;
            document.getElementById('LDAPFilter').disabled = !value;
            document.getElementById('LDAPUrl').disabled = !value;
            document.getElementById('LDAPProtocolVersion').disabled = !value;
            document.getElementById('LDAPBindUsername').disabled = !value;
            document.getElementById('LDAPBindPassword').disabled = !value;
        }

        function buildLDAPUrlFromDb() {
            //get values from database
            var scheme = '<?php echo $resultset['LDAPScheme']; ?>';
            var host = '<?php echo $resultset['LDAPHost']; ?>';
            var port = '<?php echo $resultset['LDAPPort']; ?>';
            var baseDN = '<?php echo $resultset['LDAPBaseDN']; ?>';
            var usernameAttribute = '<?php echo $resultset['LDAPUsernameAttribute']; ?>';
            var searchScope = '<?php echo $resultset['LDAPSearchScope']; ?>';
            var filter = '<?php echo $resultset['LDAPFilter']; ?>';
            buildLDAPUrl(scheme, host, port, baseDN, usernameAttribute, searchScope, filter);
        }

        function buildLDAPUrlFromForm() {
            buildLDAPUrl(
                    document.getElementById('LDAPScheme').value,
                    document.getElementById('LDAPHost').value,
                    document.getElementById('LDAPPort').value,
                    document.getElementById('LDAPBaseDN').value,
                    document.getElementById('LDAPUsernameAttribute').value,
                    document.getElementById('LDAPSearchScope').value,
                    document.getElementById('LDAPFilter').value);
        }

        function buildLDAPUrl(scheme, host, port, baseDN, usernameAttribute, searchScope, filter) {
            //fill out defaults for those which are empty
            if (scheme == '')
                scheme = 'ldaps';
            if (host == '')
                host = 'localhost';
            if (port == '')
                port = 389;
            if (baseDN == '')
                baseDN = 'dc=yourOrganisation, dc=com, ou=yourOrganisationalUnit';
            if (usernameAttribute == '')
                usernameAttribute = 'uid';
            if (searchScope == '')
                searchScope = 'base';

            //combine into one string
            var url = scheme + '://' + host + ':' + port + '/' + baseDN + '?' + usernameAttribute + '?'
                    + searchScope;

            if (filter != '')
                url += '?' + filter;

            //set in the form
            document.getElementById('LDAPUrl').value = url;
        }

        function fillOutLDAPFieldsFromUrl() {

            //get the url from the form
            var url = document.getElementById('LDAPUrl').value;

            if (url.indexOf('ldaps') == 0)
                document.getElementById('LDAPScheme').selectedIndex = 1;
            else
                document.getElementById('LDAPScheme').selectedIndex = 0;

            //find the host
            var pos1 = url.indexOf('://') + 2;
            if (pos1 == -1)
                return false;
            var pos2 = url.indexOf(':', pos1 + 1);
            if (pos2 == -1)
                return;
            document.getElementById('LDAPHost').value = url.substring(pos1 + 1, pos2);

            //find the port
            var pos3 = url.indexOf('/', pos2 + 1);
            if (pos3 == -1)
                return false;
            document.getElementById('LDAPPort').value = url.substring(pos2 + 1, pos3);

            //find the base dn
            var pos4 = url.indexOf('?', pos3 + 1);
            if (pos4 == -1)
                return false;
            document.getElementById('LDAPBaseDN').value = url.substring(pos3 + 1, pos4);

            //find the username attribute
            var pos5 = url.indexOf('?', pos4 + 1);
            if (pos5 == -1)
                return false;
            document.getElementById('LDAPUsernameAttribute').value = url.substring(pos4 + 1, pos5);

            //find the search scope	
            var pos6 = url.indexOf('?', pos5 + 1);
            if (pos6 == -1)
                pos6 = url.length;
            var searchScope = url.substring(pos5 + 1, pos6);
            if (searchScope == 'one')
                document.getElementById('LDAPSearchScope').selectedIndex = 1;
            else if (searchScope == 'sub')
                document.getElementById('LDAPSearchScope').selectedIndex = 2;
            else
                document.getElementById('LDAPSearchScope').selectedIndex = 0;
            if (pos6 == -1)
                return true;

            //the filter
            document.getElementById('LDAPFilter').value = url.substring(pos6 + 1, url.length);
            return true;
        }

        function onSubmit() {
            if (document.configurationForm.LDAPEntryMethod.value != 'normal') {
                if (!fillOutLDAPFieldsFromUrl()) {
                    alert('There was an error parsing the LDAP Url. Please correct it and try again.');
                    return;
                }
            }

            if (document.getElementById('useLDAPCheck').checked)
                document.getElementById('useLDAP').value = 1;
            else
                document.getElementById('useLDAP').value = 0;

            //re-enable the fields just before submitting because otherwise they are not send in mozilla
            enableLDAP(true);

            //submit the form
            document.configurationForm.submit();
        }

    </script>
    <body class="skin-blue sidebar-mini" onload="enableLDAP(<?php echo $resultset["useLDAP"] ?>);">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <form class="form-horizontal" action="config_action.php" name="configurationForm" method="post">
                        <input type="hidden" name="action" value="edit">
                        <div class="callout callout-info">
                            <h4>Configuration Parameters:</h4>
                            <p>
                                This form allows you to change the basic operating parameters of timesheet.php.
                                Please be careful here, as errors may cause pages not to display properly.
                                Somewhere in one of these, you should include the placeholder %commandMenu%.
                                This is where timesheet.php will place the menu options.
                            </p>
                        </div>
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">LDAP:</label>
                                    <div class="col-sm-10">
                                        <div class="row no-margin">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="useLDAPCheck" id="useLDAPCheck" onclick="enableLDAP(this.checked);" <?php if ($resultset['useLDAP'] == 1) echo "checked"; ?>>Use LDAP for authentication</input>
                                                    <input type="hidden" name="useLDAP" id="useLDAP"></input>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row no-margin">
                                            <fieldset>
                                                <legend>Connection Details</legend>
                                                <div class="row top10">
                                                    <div class="col-sm-3">
                                                        Data entry style:
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select id="LDAPEntryMethod" class="form-control input-sm" name="LDAPEntryMethod" onChange="onChangeLDAPEntryMethod();">
                                                            <option value="normal" selected>Normal</option>
                                                            <option value="advanced">RFC 2255 URL</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="normalLDAPEntry">
                                                    <div class="row top10">

                                                        <div class="col-sm-3">
                                                            Scheme:
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <select class="form-control input-sm" id="LDAPScheme" name="LDAPScheme">
                                                                <option value="ldap" <?php if ($resultset["LDAPScheme"] == "ldap") print "selected"; ?>>LDAP</option>
                                                                <option value="ldaps" <?php if ($resultset["LDAPScheme"] == "ldaps") print "selected"; ?>>LDAPS</option>
                                                            </select>
                                                            (LDAP=Non SSL, LDAPS=Use SSL)
                                                        </div>
                                                    </div>

                                                    <div class="row top10">
                                                        <div class="col-sm-3">
                                                            Host:
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input id="LDAPHost" class="form-control input-sm" name="LDAPHost" type="text" value="<?php echo $resultset['LDAPHost']; ?>"></input>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            Post:
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input id="LDAPPort" class="form-control input-sm" name="LDAPPort" type="text" size="10" maxlength="10" value="<?php echo $resultset['LDAPPort']; ?>"></input>
                                                        </div>
                                                    </div>
                                                    <div class="row top10">
                                                        <div class="col-sm-3">
                                                            LDAP search base (Distinguished Name):
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input id="LDAPBaseDN" class="form-control input-sm" type="text" name="LDAPBaseDN" value="<?php echo $resultset["LDAPBaseDN"]; ?>"></input>
                                                        </div>
                                                    </div>
                                                    <div class="row top10">
                                                        <div class="col-sm-3">
                                                            Username attribute to query:
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input id="LDAPUsernameAttribute" class="form-control input-sm" name="LDAPUsernameAttribute" type="text" value="<?php echo $resultset["LDAPUsernameAttribute"]; ?>" size="30"></input>
                                                        </div>
                                                    </div>
                                                    <div class="row top10">
                                                        <div class="col-sm-3">
                                                            Search scope:
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <select class="form-control input-sm" id="LDAPSearchScope" name="LDAPSearchScope">
                                                                <option value="base" <?php if ($resultset["LDAPSearchScope"] == "base") print "selected"; ?>>Base DN search only</option>
                                                                <option value="one" <?php if ($resultset["LDAPSearchScope"] == "one") print "selected"; ?>>One level search</option>
                                                                <option value="sub" <?php if ($resultset["LDAPSearchScope"] == "sub") print "selected"; ?>>Full sub-tree search</option>
                                                            </select>	
                                                        </div>
                                                    </div>
                                                    <div class="row top10">
                                                        <div class="col-sm-3">
                                                            Filter
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input id="LDAPFilter" class="input-sm form-control" type="text" name="LDAPFilter" value="<?php echo $resultset["LDAPFilter"]; ?>" style="width:100%;"></input>

                                                        </div>
                                                    </div>
                                                    <div class="row top10">
                                                        <div class="col-sm-3">
                                                            Protocol Version:
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <select class="form-control input-sm" id="LDAPProtocolVersion" name="LDAPProtocolVersion">
                                                                <option value="3" <?php if ($resultset["LDAPProtocolVersion"] == "3") print "selected"; ?>>3</option>
                                                                <option value="2" <?php if ($resultset["LDAPProtocolVersion"] == "2") print "selected"; ?>>2</option>
                                                                <option value="1" <?php if ($resultset["LDAPProtocolVersion"] == "1") print "selected"; ?>>1</option>
                                                            </select>	
                                                        </div>
                                                    </div>
                                                    <div class="row top10">
                                                        <div class="col-sm-12">
                                                            <span class="text-gray" nowrap><i>The following fields are normally only required for Microsoft's Active Directory LDAP Server:</i></span>
                                                        </div>
                                                    </div>
                                                    <div class="row top10">
                                                        <div class="col-sm-3">
                                                            Bind Username:
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input id="LDAPBindUsername" class="form-control input-sm" type="text" name="LDAPBindUsername" value="<?php echo $resultset["LDAPBindUsername"]; ?>"></input>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            Bind Password:
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input id="LDAPBindPassword" class="form-control input-sm" type="password" name="LDAPBindPassword" value="<?php echo $resultset["LDAPBindPassword"]; ?>"></input>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="advancedLDAPEntry" style="display:none;">
                                                    <div class="row top10">
                                                        <div class="col-sm-3">
                                                            RFC 2255 URL:
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input id="LDAPUrl" class="form-control input-sm" name="LDAPUrl" type="text" value=""></input>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        locale:
                                    </label>
                                    <div class="col-sm-10">
                                        <div class="row no-margin">
                                            <div class="col-sm-12">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="localeReset" value="off" valign="absmiddle" onclick="document.configurationForm.locale.disabled = (this.checked);">Reset</input>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-margin top10">
                                            <div class="col-sm-12">
                                                The locale in which you want timesheet.php to work. This affects regional settings. Leave it blank if you want to use the system locale. An example locale is <code>en_AU</code>, for Australia.

                                            </div>
                                        </div>
                                        <div class="row no-margin top10">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control input-sm" name="locale" size="75" maxlength="254" value="<?php echo htmlentities(trim(stripslashes($resultset["locale"]))); ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        Time Zone:
                                    </label>
                                    <div class="col-sm-10">
                                        <div class="row no-margin">
                                            <div class="col-sm-12">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="timezoneReset" value="off" onclick="document.configurationForm.timezone.disabled = (this.checked);">Reset</input>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-margin top10">
                                            <div class="col-sm-12">
                                                The timezone to use when generating dates. Leave it blank to use the system timezone. An example timezone is <code>Australia/Melbourne</code>.
                                            </div>
                                        </div>
                                        <div class="row no-margin top10">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control input-sm" name="timezone" size="75" maxlength="254" value="<?php echo htmlentities(trim(stripslashes($resultset["timezone"]))); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        Time Format:
                                    </label>
                                    <div class="col-sm-10">
                                        <div class="row no-margin">
                                            <div class="col-sm-12">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="timeformatReset" value="off" onclick="document.configurationForm.timeformat.disabled = (this.checked);">Reset</input>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-margin top10">
                                            <div class="col-sm-12">
                                                The format in which times should be displayed.	For example:<br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;<i> 12 hour format:</i><code>&nbsp;5:35 pm</code>
                                                &nbsp;&nbsp;&nbsp;&nbsp;<i> 24 hour format:</i><code>&nbsp;17:35</code>   </div>
                                        </div>
                                        <div class="row no-margin top10">
                                            <div class="col-sm-12">
                                                <select class=" form-control input-sm" name="timeformat" style="width: 100%;">
                                                    <?php if ($resultset["timeformat"] == "12") { ?>
                                                        <option value="12" selected>12 hour format</option>
                                                        <option value="24">24 hour format</option>
                                                    <?php } else { ?>
                                                        <option value="12">12 hour format</option>
                                                        <option value="24" selected>24 hour format</option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        Week Start Day:
                                    </label>
                                    <div class="col-sm-10">
                                        <div class="row no-margin">
                                            <div class="col-sm-12">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="weekStartDayReset" value="off" onclick="document.configurationForm.weekstartday.disabled = (this.checked);">Reset</input>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-margin top10">
                                            <div class="col-sm-12">
                                                The starting day of the week. Some people prefer to calculate the week starting 
                                                from Monday rather than Sunday.
                                            </div>
                                        </div>
                                        <div class="row no-margin top10">
                                            <div class="col-sm-12">
                                                <select class="form-control input-sm" name="weekstartday" style="width: 100%;">					
                                                    <?php
//get the current time
                                                    $dowDate = time();

//make it sunday
                                                    $dowDate -= (24 * 60 * 60) * date("w", $dowDate);

//for each day of the week
                                                    for ($i = 0; $i < 7; $i++) {
                                                        $dowString = strftime("%A", $dowDate);
                                                        print "<option value=\"$i\"";
                                                        if ($resultset["weekstartday"] == $i)
                                                            print " selected";
                                                        print ">$dowString</option>";
                                                        //increment the day
                                                        $dowDate += (24 * 60 * 60);
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            headerhtml:
                                        </label>
                                        <div class="col-sm-10">
                                            <div class="row no-margin">
                                                <div class="col-sm-12">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="headerReset" value="off" onclick="document.configurationForm.headerhtml.disabled = (this.checked);">Reset</input>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    Additional HTML to add to the HEAD area of documents, eg. links to stylesheets.

                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    <textarea class="form-control" rows="5" cols="73" name="headerhtml" style="width: 100%;"><?php echo htmlentities(trim(stripslashes($resultset["headerhtml"]))); ?>	</textarea>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            bodyhtml:
                                        </label>
                                        <div class="col-sm-10">
                                            <div class="row no-margin">
                                                <div class="col-sm-12">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="bodyReset" value="off" onclick="document.configurationForm.bodyhtml.disabled = (this.checked);">Reset</input>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    Additional parameters to add to the BODY tag at the beginning of documents, eg. background image/colors, link colors, etc
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    <textarea class="form-control" rows="5" cols="73" name="bodyhtml"  style="width: 100%;"><?php echo htmlentities(trim(stripslashes($resultset["bodyhtml"]))); ?></textarea>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            bannerhtml:
                                        </label>
                                        <div class="col-sm-10">
                                            <div class="row no-margin">
                                                <div class="col-sm-12">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="bannerReset" value="off" onclick="document.configurationForm.bannerhtml.disabled
                                                                            = (this.checked);">Reset</input>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    The html that gets emitted at the head of every page. This is a good place to insert the placeholder %commandMenu%. You may also want to include the placeholder %username% as part of a welcome message.
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    <textarea class="form-control" rows="5" cols="73" name="bannerhtml" style="width: 100%;"><?php echo htmlentities(trim(stripslashes($resultset["bannerhtml"]))); ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            footerhtml:
                                        </label>
                                        <div class="col-sm-10">
                                            <div class="row no-margin">
                                                <div class="col-sm-12">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="footerReset" value="off" onclick="document.configurationForm.footerhtml.disabled = (this.checked);">Reset</input>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    HTML to add to the bottom of every page. If you include %time%, %date%, and %timezone% here, it will print the time and date the page was loaded.
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    <textarea class="form-control" rows="5" cols="73" name="footerhtml" style="width: 100%;"><?php echo htmlentities(trim(stripslashes($resultset["footerhtml"]))); ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            errorhtml:
                                        </label>
                                        <div class="col-sm-10">
                                            <div class="row no-margin">
                                                <div class="col-sm-12">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="errorReset" value="off" onclick="document.configurationForm.errorhtml.disabled = (this.checked);">Reset</input>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    This is what is printed out when a form is improperly filled out. %errormsg% is replaced by the actual error itself.
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    <textarea class="form-control" rows="5" cols="73" name="errorhtml" style="width: 100%;"><?php echo htmlentities(trim(stripslashes($resultset["errorhtml"]))); ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            tablehtml:
                                        </label>
                                        <div class="col-sm-10">
                                            <div class="row no-margin">
                                                <div class="col-sm-12">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="tableReset" value="off" onclick="document.configurationForm.tablehtml.disabled = (this.checked);">Reset</input>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    Additional parameters to add to the TABLE tag when displaying sheets, calenders, etc. This is often used to set the background color or background image of the table.
                                                </div>
                                            </div>
                                            <div class="row no-margin top10">
                                                <div class="col-sm-12">
                                                    <textarea class="form-control" rows="5" cols="73" name="tablehtml" style="width: 100%;"><?php echo htmlentities(trim(stripslashes($resultset["tablehtml"]))); ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer text-center">
                                <input type="button" class="btn btn-info btn-sm" value="Submit Changes" name="submitButton" id="submitButton" onClick="onSubmit();"></input>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </body>
</HTML>
