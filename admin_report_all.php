<?php
// $Header: /cvsroot/tsheet/timesheet.php/admin_report_all.php,v 1.5 2005/05/23 10:42:46 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//load local vars from superglobals
$uid = isset($_REQUEST["uid"]) ? $_REQUEST["uid"] : 1;
$orderby = isset($_REQUEST["orderby"]) ? $_REQUEST["orderby"] : "username";

//define the command menu
include("timesheet_menu.inc");

// Calculate the previous month.
$next_month = $month + 1;
$next_year = $year;
$prev_month = $month - 1;
$prev_year = $year;

//rollover year forward
if (!checkdate($next_month, 1, $next_year)) {
    $next_month -= 12;
    $next_year ++;
}

//rollover year back
if (!checkdate($prev_month, 1, $prev_year)) {
    $prev_month += 12;
    $prev_year --;
}
?>
<html>
    <head><title>Timesheet.php Report: All hours this month</title>
        <?php include ("header.inc"); ?>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <div class="box">
                        <div class="box-body">
                            <div class="row no-margin">
                                <?php echo date('F Y', mktime(0, 0, 0, $month, 1, $year)) ?>
                                <div class="btn-group pull-right">
                                    <a class="btn btn-sm btn-info" href="<?php echo $_SERVER['PHP_SELF']; ?>?uid=<?php echo $uid; ?>&month=<?php echo $prev_month; ?>&year=<?php echo $prev_year; ?>">Prev</a>
                                    <a class="btn btn-sm btn-info" href="<?php echo $_SERVER['PHP_SELF'] ?>?uid=<?php echo $uid; ?>&month=<?php echo $next_month; ?>&year=<?php echo $next_year; ?>">Next</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?orderby=username&month=<?php echo $month; ?>&year=<?php echo $year; ?>">Username</a></th>
                                        <th>Hours</th>
                                        <th class="odd">
                                            <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?orderby=<?php echo $PROJECT_TABLE; ?>.proj_id&month=<?php echo $month; ?>&year=<?php echo $year; ?>">Project</a> | 
                                            <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?orderby=<?php echo $TASK_TABLE; ?>.task_id&month=<?php echo $month; ?>&year=<?php echo $year; ?>">Task</a></b>
                                        </th>
                                    </tr>	
                                </thead>
                                <tbody>
                                    <?php
                                    $query = "select distinct first_name, last_name, $USER_TABLE.username, $PROJECT_TABLE.title, $PROJECT_TABLE.proj_id, " .
                                            "$TASK_TABLE.name, $TASK_TABLE.task_id " .
                                            "FROM $USER_TABLE, $PROJECT_TABLE, $TASK_TABLE, $ASSIGNMENTS_TABLE, $TASK_ASSIGNMENTS_TABLE WHERE " .
                                            "$ASSIGNMENTS_TABLE.proj_id = $PROJECT_TABLE.proj_id and $TASK_ASSIGNMENTS_TABLE.task_id = $TASK_TABLE.task_id " .
                                            "AND $PROJECT_TABLE.proj_id = $TASK_TABLE.proj_id AND " .
                                            "$ASSIGNMENTS_TABLE.username = $USER_TABLE.username and $USER_TABLE.username NOT IN ('admin','guest') ORDER BY $orderby";

                                    list ($qh, $num) = dbQuery($query);
                                    $last_username = "";

                                    if ($num == 0) {
                                        print "	<tr>\n";
                                        print "		<td align=\"center\">\n";
                                        print "			<i><br>No hours recorded.<br><br></i>\n";
                                        print "		</td>\n";
                                        print "	</tr>\n";
                                    } else {
                                        while ($name_data = dbResult($qh)) {
                                            $query = "SELECT sec_to_time(sum(unix_timestamp(end_time) - unix_timestamp(start_time))) AS diff " .
                                                    "FROM $TIMES_TABLE WHERE " .
                                                    "start_time >= '$year-$month-1' AND end_time < '$next_year-$next_month-1' and end_time > 0 " .
                                                    "and uid='$name_data[username]' AND task_id=$name_data[task_id] and proj_id=$name_data[proj_id]";

                                            list($qh2, $num2) = dbQuery($query);
                                            if ($num2 > 0)
                                                $time_data = dbResult($qh2);

                                            print "<tr>\n";

                                            if ($last_username != $name_data["username"]) {
                                                $last_username = $name_data["username"];
                                                print "<td class=\"calendar_cell_middle\">$name_data[first_name] $name_data[last_name]</TD>\n";
                                                print "<td class=\"calendar_cell_middle\"><A HREF=\"admin_report_specific_user.php?uid=$name_data[username]&month=$month&year=$year\">$name_data[username]</A></TD>\n";
                                            } else {
                                                print "<td class=\"calendar_cell_middle\">&nbsp;</td>\n";
                                                print "<td class=\"calendar_cell_middle\">&nbsp;</td>\n";
                                            }

                                            print "<td class=\"calendar_cell_middle\" align=\"center\">";
                                            if ($num2 > 0 && isset($time_data["diff"]))
                                                echo $time_data["diff"];
                                            else
                                                print "&nbsp;";
                                            print "</td>\n\n";

                                            $projectTitle = stripslashes($name_data["title"]);
                                            $taskName = stripslashes($name_data["name"]);
                                            ?>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#myModal_<?php echo $name_data['proj_id']; ?>"><?php echo $projectTitle; ?></a> | <a href="#" data-toggle="modal" data-target="#myModal_t_<?php echo $name_data['task_id'] ?>"><?php echo $taskName; ?></a>
                                            <div id="myModal_<?php echo $name_data['proj_id']; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog modal-default">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                            <h4 class="modal-title">Project info</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <?php
                                                            $proj_id = $name_data['proj_id'];
                                                            $query_project = "select distinct title, description," .
                                                                    "DATE_FORMAT(start_date, '%M %d, %Y') as start_date," .
                                                                    "DATE_FORMAT(deadline, '%M %d, %Y') as deadline," .
                                                                    "proj_status, proj_leader " .
                                                                    "from $PROJECT_TABLE " .
                                                                    "where $PROJECT_TABLE.proj_id=$proj_id";

                                                            $query_p = "select distinct $PROJECT_TABLE.title, $PROJECT_TABLE.proj_id, $PROJECT_TABLE.client_id, $CLIENT_TABLE.organisation, " .
                                                                    "$PROJECT_TABLE.description, DATE_FORMAT(start_date, '%M %d, %Y') as start_date, DATE_FORMAT(deadline, '%M %d, %Y') as deadline, " .
                                                                    "$PROJECT_TABLE.proj_status, http_link " .
                                                                    "from $PROJECT_TABLE, $CLIENT_TABLE, $USER_TABLE " .
                                                                    "where $PROJECT_TABLE.proj_id=$proj_id  ";

//set up query
                                                            $query_p = "select distinct $PROJECT_TABLE.title, $PROJECT_TABLE.proj_id, $PROJECT_TABLE.client_id, " .
                                                                    "$CLIENT_TABLE.organisation, $PROJECT_TABLE.description, " .
                                                                    "DATE_FORMAT(start_date, '%M %d, %Y') as start_date, " .
                                                                    "DATE_FORMAT(deadline, '%M %d, %Y') as deadline, " .
                                                                    "$PROJECT_TABLE.proj_status, http_link, proj_leader " .
                                                                    "FROM $PROJECT_TABLE, $CLIENT_TABLE, $USER_TABLE " .
                                                                    "WHERE $PROJECT_TABLE.proj_id=$proj_id AND " .
                                                                    "$CLIENT_TABLE.client_id=$PROJECT_TABLE.client_id " .
                                                                    "ORDER BY $PROJECT_TABLE.proj_id";

                                                            list($qh_p, $num_p) = dbQuery($query_p);
                                                            if ($num_p > 0) {

                                                                //get the current record
                                                                $data_p = dbResult($qh_p);

                                                                //strip slashes
                                                                $data_p["title"] = stripslashes($data_p["title"]);
                                                                $data_p["organisation"] = stripslashes($data_p["organisation"]);
                                                                $data_p["description"] = stripslashes($data_p["description"]);

                                                                list($billqh, $bill_num) = dbquery("select sum(unix_timestamp(end_time) - unix_timestamp(start_time)) as total_time, " .
                                                                        "sum(bill_rate * ((unix_timestamp(end_time) - unix_timestamp(start_time))/(60*60))) as billed " .
                                                                        "from $TIMES_TABLE, $USER_TABLE " .
                                                                        "where end_time > 0 AND $TIMES_TABLE.proj_id = $data_p[proj_id] AND $USER_TABLE.username = $TIMES_TABLE.uid ");
                                                                $bill_data = dbResult($billqh);
                                                                ?>
                                                                <div class="row no-margin">
                                                                            <div class="col-xs-12">
                                                                                <h4 class="box-title">
                                                                                    <?php
                                                                                    if ($data_p["http_link"] != "") {
                                                                                        ?>
                                                                                        <a href="<?php echo $data_p['http_link'] ?>"><span><?php echo $data_p['title'] ?></span></a>
                                                                                        <?php
                                                                                    } else {
                                                                                        ?>
                                                                                        <span><?php echo $data_p['title'] ?></span>
                                                                                        <?php
                                                                                    }
                                                                                    print "&nbsp;&nbsp;<span>&lt;$data_p[proj_status]&gt;</span>"
                                                                                    ?>
                                                                                </h4>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row no-margin">
                                                                            <div class="col-xs-12">
                                                                                <label>Description:</label><?php echo $data_p["description"]; ?>
                                                                            </div>

                                                                        </div>
                                                                        <div class="row no-margin">
                                                                            <div class="col-xs-12">
                                                                                <label>Client:</label> <?php echo $data_p["organisation"]; ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row no-margin">
                                                                            <div class="col-xs-6">
                                                                                <label>Total time:</label> <?php echo (isset($bill_data["total_time"]) ? formatSeconds($bill_data["total_time"]) : "0h 0m"); ?>
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <?php if ($authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) { ?>
                                                                                    <label>Total bill:</label> <b>$<?php echo (isset($bill_data["billed"]) ? $bill_data["billed"] : "0.00"); ?></b>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row no-margin">
                                                                            <div class="col-xs-12">
                                                                                <div class="row no-margin"><label>Project Leader:</label><?php echo $data_p['proj_leader'] ?> </div>
                                                                                <?php
                                                                                //display assigned users      
                                                                                list($qh2, $num_workers) = dbQuery("select distinct username from $ASSIGNMENTS_TABLE where proj_id = $data_p[proj_id]");
                                                                                if ($num_workers == 0) {
                                                                                    ?>
                                                                                    <font size=\"-1\">Nobody assigned to this project</font>
                                                                                    <?php
                                                                                } else {
                                                                                    $workers = '';
                                                                                    ?>
                                                                                    <div class="row no-margin">
                                                                                        <label> Assigned Users: </label>

                                                                                        <?php
                                                                                        for ($k = 0; $k < $num_workers; $k++) {
                                                                                            $worker = dbResult($qh2);
                                                                                            $workers .= "$worker[username], ";
                                                                                        }

                                                                                        //$workers = ereg_replace(", $", "", $workers);
                                                                                        ?>

                                                                                        <?php echo $workers; ?>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row no-margin">

                                                                            <?php
                                                                            if (isset($data_p["start_date"]) && $data_p["start_date"] != '' && $data_p["deadline"] != '') {
                                                                                ?>
                                                                                <div class="col-xs-6">

                                                                                    <label>Start:</label> 
                                                                                    <?php echo date('d-m-Y', strtotime($data_p['start_date'])); ?>
                                                                                </div>
                                                                                <div class="col-xs-6">
                                                                                    <label>Deadline:</label> 
                                                                                    <?php echo date('d-m-Y', strtotime($data_p['deadline'])); ?>
                                                                                </div>
                                                                                <?php
                                                                            }
                                                                            ?>		

                                                                        </div>
                                                                        <div class="no-margin row">
                                                                            <div class="col-xs-12"
                                                                                 <div class="project_task_list">
                                                                                    <a href="task_maint.php?proj_id=<?php echo $data["proj_id"]; ?>"><span>Tasks</span></a>
                                                                                    <?php
//                                                                                    //get tasks
//                                                                                    list($qh3, $num_tasks) = dbQuery("select name, task_id FROM $TASK_TABLE WHERE proj_id=$data[proj_id]");
//
//                                                                                    //are there any tasks?
//                                                                                    if ($num_tasks > 0) {
//                                                                                        while ($task_data = dbResult($qh3)) {
//                                                                                            $taskName = str_replace(" ", "&nbsp;", $task_data["name"]);
//                                                                                            print "<a href=\"javascript:void(0)\" onclick=window.open(\"task_info.php?proj_id=$data[proj_id]&task_id=$task_data[task_id]\",\"TaskInfo\",\"location=0,directories=no,status=no,menubar=no,resizable=1,width=550,height=220\")>$taskName</a><br>";
//                                                                                        }
//                                                                                    } else
//                                                                                        print "None.";
                                                                                    ?>	
                                                                                </div>                                                                                                                                                                                                                                                         
                                                                            </div>
                                                                        </div>
                                                                <?php
                                                            }
                                                            ?>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <?php
                                                $task_id = $name_data['task_id'];
                                                $query_task = "select distinct task_id, name, description,status, " .
                                                        "DATE_FORMAT(assigned, '%M %d, %Y') as assigned," .
                                                        "DATE_FORMAT(started, '%M %d, %Y') as started," .
                                                        "DATE_FORMAT(suspended, '%M %d, %Y') as suspended," .
                                                        "DATE_FORMAT(completed, '%M %d, %Y') as completed " .
                                                        "from $TASK_TABLE " .
                                                        "where $TASK_TABLE.task_id=$task_id " .
                                                        "order by $TASK_TABLE.task_id";

//get the proj_id for this task
                                                if (!isset($proj_id)) {
                                                    list($qh_m, $num_m) = $proj_id = dbQuery("SELECT proj_id FROM $TASK_TABLE where task_id='$task_id'");
                                                    $results_m = dbResult($qh);
                                                    $proj_id = $results["proj_id"];
                                                }

                                                $query_project = "select distinct title, description," .
                                                        "DATE_FORMAT(start_date, '%M %d, %Y') as start_date," .
                                                        "DATE_FORMAT(deadline, '%M %d, %Y') as deadline," .
                                                        "proj_status, proj_leader " .
                                                        "from $PROJECT_TABLE " .
                                                        "where $PROJECT_TABLE.proj_id=$proj_id";
                                                ?>
                                                <div id="myModal_t_<?php echo $name_data['task_id']; ?>" class="modal fade" role="dialog">
                                                    <div class="modal-dialog modal-default">


                                                        <div class="modal-content text-left">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                                <h4 class="modal-title">Task info</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <?php
                                                                list($qh_m, $num_m) = dbQuery($query_task);
                                                                if ($num_m > 0) {
                                                                    $data_task_ = dbResult($qh_m);
                                                                    ?>
                                                                    <div class="row no-margin">
                                                                        <div class="col-sm-10 col-xs-12">
                                                                            <h4 class="box-title text-blue"><?php echo stripslashes($data_task_["name"]); ?></h4>
                                                                        </div>

                                                                        <div class="col-sm-2 col-xs-12" style="padding: 12px 0px 0px 0px;">
                                                                            <small>&lt;<?php echo $data_task_["status"]; ?>&gt;</small>
                                                                        </div>

                                                                    </div>
                                                                    <div class="row no-margin">
                                                                        <div class="col-sm-12 col-xs-12">
                                                                            <?php echo stripslashes($data_task_["description"]); ?>

                                                                        </div>
                                                                    </div>
                                                                    <div class="row no-margin">
                                                                        <div class="col-sm-12 col-xs-12">
                                                                            <label>Assigned persons:</label><br>
                                                                            <?php
                                                                            //get assigned users
                                                                            list($qh3_, $num_3_) = dbQuery("select username, task_id from $TASK_ASSIGNMENTS_TABLE where task_id=$task_id");
                                                                            if ($num_3_ > 0) {
                                                                                while ($data_3_ = dbResult($qh3_)) {
                                                                                    print "$data_3_[username], ";
                                                                                }
                                                                            } else {
                                                                                print "<i>None</i>";
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                <?php }
                                                                ?>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>


                                        </td>
                                        </tr>
                                        <?php
//                                        print "<td class=\"odd\"><a href=\"javascript:void(0)\" ONCLICK=window.open(\"proj_info.php?proj_id=$name_data[proj_id]\",\"Info\",\"location=0,directories=no,status=no,menubar=no,resizable=1,scrollbar=yes,width=580,height=200\") class=\"outer_table_action\">$projectTitle</A> " .
//                                                " | <a href=\"javascript:void(0)\" ONCLICK=window.open(\"task_info.php?proj_id=$name_data[proj_id]&task_id=$name_data[task_id]\",\"TaskInfo\",\"location=0,directories=no,status=no,scrollbar=yes,menubar=no,resizable=1,width=580,height=220\")>$taskName</A></TD>\n";
//                                        print "</tr>\n";
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </body>
</HTML>
