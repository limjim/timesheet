<?php
//  //Get the result set for the config set 1
//  list($qhq, $numq) = dbQuery("select bannerhtml from $CONFIG_TABLE where config_set_id = '1'");
//  $configData = dbResult($qhq);
//	parse_and_echo(stripslashes($configData["bannerhtml"]));
?>

<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <div class="nav-collapse collapse" style="font-size: 11px;">
                <ul class="nav navbar-nav">
                    <?php
                    parse_and_echo("%commandmenu%");
                    ?>
                </ul>
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php parse_and_echo("%username%"); ?> <i class="caret"></i></a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="changepwd.php">Change Password</a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="logout.php?logout=true">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
