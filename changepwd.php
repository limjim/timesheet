<?php
// Authenticate
if (!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn()) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);
$loggedInUser = strtolower($_SESSION['loggedInUser']);

$passwd1 = "";
$passwd2 = "";
$old_pass = "";

//load local vars from superglobals
if (isset($_POST["action"])) {
    if (!isset($_POST["passwd1"]) || !isset($_POST["passwd2"]) || !isset($_POST["old_pass"]))
        errorPage("Please fill out all fields.");
    $passwd1 = $_POST['passwd1'];
    $passwd2 = $_POST['passwd2'];
    $old_pass = $_POST['old_pass'];
}

//get todays values
$today = time();
$today_year = date("Y", $today);
$today_month = date("n", $today);
$today_day = date("j", $today);

//define the command menu
include("timesheet_menu.inc");

//check for guest user
if ($loggedInUser == 'guest')
    $errormsg = "Guest may not change password.";

//check that passwords match
if ($passwd1 != $passwd2)
    $errormsg = "Passwords do not match, please try again";

if (empty($errormsg) && !empty($old_pass)) {
    $qh = mysql_query("select password, $DATABASE_PASSWORD_FUNCTION('$old_pass') from $USER_TABLE where username='$contextUser'") or die("Unable to select " . mysql_error());
    list($check1, $check2) = mysql_fetch_row($qh);
    if ($check1 != $check2) {
        $errormsg = "Wrong password, sorry!";
    } else {
        $qh = mysql_query("update $USER_TABLE set password=$DATABASE_PASSWORD_FUNCTION('$passwd1') where username='$contextUser'");
        Header("Location: calendar.php");
        exit;
    }
}

//if errors, redirect to an error page.
if (!empty($errormsg)) {
    Header("Location: error.php?errormsg=$errormsg");
    exit;
}
?>
<html>
    <head>
        <title>Change Password for user <?php echo $contextUser; ?></title>
        <?php include ("header.inc"); ?>
    </head>
    <body class="skin-blue sidebar-mini" >
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <div class="row" style="margin-top: 40px;">
                        <div class="col-md-offset-3 col-md-6 col-xs-12">
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Change password</h3>
                                </div>
                                <form class="form-horizontal" action="changepwd.php" method="post">
                                    <input type="hidden" name="action" value="changePassword" />
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="old_pass" class="col-sm-3 control-label">Old password</label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" id="old_pass" name="old_pass" placeholder="Old password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="new_pass" class="col-sm-3 control-label">New password</label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" id="new_pass" name="passwd1" placeholder="New password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="comfirm_pass" class="col-sm-3 control-label">New Password (again)</label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" id="comfirm_pass" name="passwd2" placeholder="New password (again)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <input type="submit" value="Change!" class="btn btn-info pull-right">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <?php
//        include ("footer.inc");
        ?>
    </BODY>
</HTML>
