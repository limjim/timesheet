<?php
// $Header: /cvsroot/tsheet/timesheet.php/popup.php,v 1.11 2005/05/17 03:38:37 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn()) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

if (empty($contextUser))
    errorPage("Could not determine the context user");

//load local vars from superglobals
$year = $_REQUEST["year"];
$month = $_REQUEST["month"];
$day = $_REQUEST["day"];
$destination = $_REQUEST["destination"];
$proj_id = isset($_REQUEST["proj_id"]) ? $_REQUEST["proj_id"] : 0;
$task_id = isset($_REQUEST["task_id"]) ? $_REQUEST["task_id"] : 0;
$client_id = isset($_REQUEST["client_id"]) ? $_REQUEST["client_id"] : 0;

//get todays values
$today = time();
$todayYear = date("Y", $today);
$todayMonth = date("n", $today);
$todayDay = date("j", $today);

//check that the client id is valid
if ($client_id == 0 || empty($client_id))
    $client_id = getFirstClient();

//check that project id is valid
if ($proj_id == 0)
    $task_id = 0;

//calculate tomorrow and yesterday for "prev" & "next" buttons
$yesterday = mktime(0, 0, 0, $month, $day, $year) - 24 * 60 * 60;
$tomorrow = mktime(0, 0, 0, $month, $day, $year) + 24 * 60 * 60;

function getDailyTimes($month, $day, $year, $id, $proj_id) {
    include("table_names.inc");
    list($qhq, $numq) = dbQuery("select timeformat from $CONFIG_TABLE where config_set_id = '1'");
    $configData = dbResult($qhq);

    $query = "select date_format(start_time,'%d') as day_of_month, trans_num, ";

    if ($configData["timeformat"] == "12")
        $query .= "date_format(end_time, '%l:%i%p') as endd, date_format(start_time, '%l:%i%p') as start, ";
    else
        $query .= "date_format(end_time, '%k:%i') as endd, date_format(start_time, '%k:%i') as start, ";
    $query .= "unix_timestamp(end_time) - unix_timestamp(start_time) as diff_sec, " .
            "unix_timestamp(start_time) as start_time, " .
            "unix_timestamp(end_time) as end_time, " .
            "end_time as end_time_str, " .
            "start_time as start_time_str, " .
            "$PROJECT_TABLE.title as project_title, " .
            "$TASK_TABLE.name, $TIMES_TABLE.proj_id, $TIMES_TABLE.task_id " .
            "FROM $TIMES_TABLE, $TASK_TABLE, $PROJECT_TABLE " .
            "WHERE $TASK_TABLE.proj_id=$PROJECT_TABLE.proj_id AND " .
            "uid='$id' AND ";

    $query .= "$TASK_TABLE.task_id = $TIMES_TABLE.task_id AND " .
            "((start_time >= '$year-$month-$day 00:00:00' AND start_time <= '$year-$month-$day 23:59:59') " .
            " OR (end_time >= '$year-$month-$day 00:00:00' AND end_time <= '$year-$month-$day 23:59:59') " .
            " OR (start_time < '$year-$month-$day 00:00:00' AND end_time > '$year-$month-$day 23:59:59')) " .
            " order by day_of_month, start_time";

    list($my_qh, $num) = dbQuery($query);
    return array($num, $my_qh);
}

//include date input classes
include "form_input.inc";

list($num, $qh) = getDailyTimes($month, $day, $year, $contextUser, $proj_id);
?>
<html>
    <head>
        <title>Update timesheet for <?php echo $contextUser; ?></title>
        <?php
        include("header.inc");
        include("client_proj_task_javascript.inc");
        ?>
        <script language="Javascript">

            function resizePopupWindow() {
                //now resize the window
                var outerTable = document.getElementById('outer_table');
                var newWidth = outerTable.offsetWidth + window.outerWidth - window.innerWidth;
                var newHeight = outerTable.offsetHeight + window.outerHeight - window.innerHeight;
                window.resizeTo(newWidth, newHeight);
            }

        </script>
    </HEAD>
    <body style="margin: 0;"  onload="doOnLoad();">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            Clock On / Off Now
                        </h3>
                        <div class="box-tools">
                            <?php echo strftime("%A %B %d, %Y", mktime(0, 0, 0, $month, $day, $year)); ?>
                        </div>
                    </div>
                    <form class="form-horizontal" action="action.php" method="post" name="mainForm" id="theForm">					
                        <input type="hidden" name="year" value="<?php echo $year; ?>">
                        <input type="hidden" name="month" value="<?php echo $month; ?>">
                        <input type="hidden" name="day" value="<?php echo $day; ?>">
                        <input type="hidden" id="client_id" name="client_id" value="<?php echo $client_id; ?>">
                        <input type="hidden" id="proj_id" name="proj_id" value="<?php echo $proj_id; ?>">
                        <input type="hidden" id="task_id" name="task_id" value="<?php echo $task_id; ?>">
                        <input type="hidden" name="fromPopupWindow" value="true">
                        <input type="hidden" name="origin" value="<?php echo $_SERVER["PHP_SELF"]; ?>">
                        <input type="hidden" name="destination" value="<?php echo $destination; ?>">
                        <div class="box-body">
                            <table width="100%" border="0" class="table_body">	

                                <tr>
                                    <td>
                                        <div id="client_" class="hidden">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Client:</label>
                                                <div class="col-md-9">
                                                    <select id="clientSelect" class="form-control input-sm" name="clientSelect" onChange="onChangeClientSelect();" />
                                                </div>
                                            </div>
                                        </div>
                                    </td>									
                                </tr>																									
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Project:</label>
                                            <div class="col-md-9">
                                                <select id="projectSelect" class="form-control input-sm" name="projectSelect" onChange="onChangeProjectSelect();"/>
                                            </div>
                                        </div>
                                    </td>									
                                </tr>																		
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Task:</label>
                                            <div class="col-md-9">
                                                <select id="taskSelect" class="form-control input-sm"  name="taskSelect" onChange="onChangeTaskSelect();"/>
                                            </div>
                                        </div>
                                    </td>									
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <div class="col-md-offset-3 col-md-4">
                                                <input type="checkbox" name="clock_on_check" id="clock_on_check" style="margin-right: 20px;" onclick="enableClockOn();">Clock on at

                                            </div>

                                            <div class="col-md-offset-1 col-md-2">
                                                <?php
                                                $hourInput = new HourInput("clock_on_time_hour");
                                                $hourInput->create(10);
                                                ?>
                                            </div>
                                            <div class="col-md-2">
                                                <?php
                                                $minuteInput = new MinuteInput("clock_on_time_min");
                                                $minuteInput->create();
                                                ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <div class="col-md-offset-3 col-md-4">
                                                <input type="checkbox" name="clock_off_check" id="clock_off_check" style="margin-right: 20px;" onclick="enableClockOff();">Clock off at
                                            </div>
                                            <div class="col-md-offset-1 col-md-2">
                                                <?php
                                                $hourInput = new HourInput("clock_off_time_hour");
                                                $hourInput->create(17);
                                                ?>
                                            </div>
                                            <div class="col-md-2">
                                                <?php
                                                $minuteInput = new MinuteInput("clock_off_time_min");
                                                $minuteInput->create();
                                                ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </div>
                        <div class="box-footer text-center">
                            <div class="btn-group">
                                <input type="button" class="btn btn-info btn-sm" value="Clock on and/or off" name="submitButton" id="submitButton" onClick="onSubmit();">
                            </div>
                        </div>
                    </form>	
                </div>
            </div>
        </div>
    </body>
</HTML>
