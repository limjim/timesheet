<?php
// $Header: /cvsroot/tsheet/timesheet.php/admin_report_specific_project.php,v 1.10 2005/05/23 10:42:46 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//load local vars from superglobals
if (isset($_REQUEST['uid']))
    $uid = $_REQUEST['uid'];
else
//get the first user from the database
    $uid = getFirstUser();

//define the command menu
include("timesheet_menu.inc");

if ($proj_id == 0)
//get the first project
    $proj_id = getFirstProject();


// Set default months
setReportDate($year, $month, $day, $next_week, $prev_week, $next_month, $prev_month, $time, $time_middle_month);

function format_seconds($seconds) {
    $temp = $seconds;
    $hour = (int) ($temp / (60 * 60));

    if ($hour < 10)
        $hour = '0' . $hour;

    $temp -= (60 * 60) * $hour;
    $minutes = (int) ($temp / 60);

    if ($minutes < 10)
        $minutes = '0' . $minutes;

    $temp -= (60 * $minutes);
    $sec = $temp;

    if ($sec < 10)
        $sec = '0' . $sec;  // Totally wierd PHP behavior.  There needs to
// be a space after the . operator for this to work.
    return "$hour:$minutes:$sec";
}

//get the project name
//	$query = "select title from $PROJECT_TABLE where proj_id='$proj_id'"; 
//	list($qh,$num) = dbQuery($query);
//	$data = dbResult($qh);
//	$project_title = $data['title'];	  
// Change the date-format for internationalization...
if ($mode == "all")
    $mode = "monthly";
if ($mode == "weekly") {
    $query = "select $TIMES_TABLE.proj_id, $TIMES_TABLE.task_id, " .
            "sec_to_time(unix_timestamp(end_time) - unix_timestamp(start_time)) as diff_time, " .
            "(unix_timestamp(end_time) - unix_timestamp(start_time)) as diff, $PROJECT_TABLE.title, $TASK_TABLE.name, " .
            "date_format(start_time, '%Y/%m/%d') as start_date, trans_num, $TIMES_TABLE.uid, " .
            "$USER_TABLE.first_name, $USER_TABLE.last_name, $TIMES_TABLE.log_message " .
            "from $USER_TABLE, $TIMES_TABLE, $PROJECT_TABLE, $TASK_TABLE " .
            "WHERE $TIMES_TABLE.uid=$USER_TABLE.username and end_time > 0 AND $TIMES_TABLE.proj_id='$proj_id' AND start_time >= '$year-$month-$day' AND " .
            "$PROJECT_TABLE.proj_id = $TIMES_TABLE.proj_id AND $TASK_TABLE.task_id = $TIMES_TABLE.task_id AND " .
            "end_time < '" . date("Y-m-d", $next_week) . "' order by $USER_TABLE.uid, task_id, start_time";
} else {
    $query = "select $TIMES_TABLE.proj_id, $TIMES_TABLE.task_id, " .
            "sec_to_time(unix_timestamp(end_time) - unix_timestamp(start_time)) as diff_time, " .
            "(unix_timestamp(end_time) - unix_timestamp(start_time)) as diff, $PROJECT_TABLE.title, $TASK_TABLE.name, " .
            "date_format(start_time, '%Y/%m/%d') as start_date, trans_num, $TIMES_TABLE.uid, " .
            "$USER_TABLE.first_name, $USER_TABLE.last_name, $TIMES_TABLE.log_message " .
            "from $USER_TABLE, $TIMES_TABLE, $PROJECT_TABLE, $TASK_TABLE " .
            "WHERE $TIMES_TABLE.uid=$USER_TABLE.username and end_time > 0 AND $TIMES_TABLE.proj_id='$proj_id' AND start_time >= '$year-$month-1' AND " .
            "$PROJECT_TABLE.proj_id = $TIMES_TABLE.proj_id AND $TASK_TABLE.task_id = $TIMES_TABLE.task_id AND " .
            "end_time < '" . date("Y-m-1", $next_month) . "' order by $USER_TABLE.uid, task_id, start_time";
}

//run the query  
list($qh, $num) = dbQuery($query);

//define working varibales  
$last_uid = -1;
$last_task_id = -1;
$total_time = 0;
$grand_total_time = 0;
?>
<html>
    <head>
        <title>Timesheet.php Report: Hours for a specific project</title>
        <?php include ("header.inc"); ?>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <form action="admin_report_specific_project.php" method="get">
                        <input type="hidden" name="month" value="<?php echo $month; ?>">
                        <input type="hidden" name="year" value="<?php echo $year; ?>">
                        <input type="hidden" name="day" value="<?php echo $day; ?>">
                        <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                        <div class="box">
                            <div class="box-body">
                                <div class="row no-margin">
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Project:</label>
                                            <div class="col-sm-10">
                                                <?php project_select_droplist($proj_id); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <?php
                                        if ($mode == "weekly")
                                            echo date('F d, Y', $time);
                                        else
                                            echo date('F Y', $time);
                                        ?>
                                    </div>
                                    <?php
                                    printPrevNext($time, $next_week, $prev_week, $next_month, $prev_month, $time_middle_month, "proj_id=$proj_id", $mode);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <?php
                                    if ($num == 0) {
                                        print "	<tr>\n";
                                        print "		<td align=\"center\">\n";
                                        print "			<i><br>No hours recorded.<br><br></i>\n";
                                        print "		</td>\n";
                                        print "	</tr>\n";
                                    } else {
                                        while ($data = dbResult($qh)) {
                                            // New project, so print out last project total time and start a new table cell.
                                            if ($last_uid != $data["uid"]) {
                                                $last_uid = $data["uid"];
                                                if ($grand_total_time) {
                                                    $formatted_time = format_seconds($total_time);
                                                    print "<tr><td colspan=\"4\" align=\"right\" class=\"calendar_totals_line_weekly_right\">" .
                                                            "Total: <span class=\"calendar_total_value_weekly\">$formatted_time</span></td></tr>\n";
                                                }

                                                print "<tr>\n\t<td colspan=\"4\" valign=\"top\" class=\"calendar_cell_disabled_right\">" .
                                                        stripslashes("$data[first_name] $data[last_name]") . "</td></tr>\n";
                                                $total_time = 0;
                                            }
                                            print "<tr>\n\t<td valign=\"top\" align=\"right\" width=\"50%\" class=\"calendar_cell_right\">\n\t";
                                            if ($last_task_id != $data["task_id"]) {
                                                $last_task_id = $data["task_id"];
                                                $current_task_name = stripslashes($data["name"]);
                                                $task_id = $data['task_id'];
                                                $query_task = "select distinct task_id, name, description,status, " .
                                                        "DATE_FORMAT(assigned, '%M %d, %Y') as assigned," .
                                                        "DATE_FORMAT(started, '%M %d, %Y') as started," .
                                                        "DATE_FORMAT(suspended, '%M %d, %Y') as suspended," .
                                                        "DATE_FORMAT(completed, '%M %d, %Y') as completed " .
                                                        "from $TASK_TABLE " .
                                                        "where $TASK_TABLE.task_id=$task_id " .
                                                        "order by $TASK_TABLE.task_id";

//get the proj_id for this task
                                                if (!isset($proj_id)) {
                                                    list($qh_m, $num_m) = $proj_id = dbQuery("SELECT proj_id FROM $TASK_TABLE where task_id='$task_id'");
                                                    $results_m = dbResult($qh);
                                                    $proj_id = $results["proj_id"];
                                                }

                                                $query_project = "select distinct title, description," .
                                                        "DATE_FORMAT(start_date, '%M %d, %Y') as start_date," .
                                                        "DATE_FORMAT(deadline, '%M %d, %Y') as deadline," .
                                                        "proj_status, proj_leader " .
                                                        "from $PROJECT_TABLE " .
                                                        "where $PROJECT_TABLE.proj_id=$proj_id";
                                                ?>
                                                <div class="row no-margin">
                                                    <a href="#" data-toggle="modal" data-target="#myModal_t_<?php echo $data['task_id'] ?>"><?php echo $current_task_name; ?></a>
                                                    <div id="myModal_t_<?php echo $data['task_id']; ?>" class="modal fade" role="dialog">
                                                        <div class="modal-dialog modal-default">


                                                            <div class="modal-content text-left">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                                    <h4 class="modal-title">Task info</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php
                                                                    list($qh_m, $num_m) = dbQuery($query_task);
                                                                    if ($num_m > 0) {
                                                                        $data_task_ = dbResult($qh_m);
                                                                        ?>
                                                                        <div class="row no-margin">
                                                                            <div class="col-sm-10 col-xs-12">
                                                                                <h4 class="box-title text-blue"><?php echo stripslashes($data_task_["name"]); ?></h4>
                                                                            </div>

                                                                            <div class="col-sm-2 col-xs-12" style="padding: 12px 0px 0px 0px;">
                                                                                <small>&lt;<?php echo $data_task_["status"]; ?>&gt;</small>
                                                                            </div>

                                                                        </div>
                                                                        <div class="row no-margin">
                                                                            <div class="col-sm-12 col-xs-12">
                                                                                <?php echo stripslashes($data_task_["description"]); ?>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row no-margin">
                                                                            <div class="col-sm-12 col-xs-12">
                                                                                <label>Assigned persons:</label><br>
                                                                                <?php
                                                                                //get assigned users
                                                                                list($qh3_, $num_3_) = dbQuery("select username, task_id from $TASK_ASSIGNMENTS_TABLE where task_id=$task_id");
                                                                                if ($num_3_ > 0) {
                                                                                    while ($data_3_ = dbResult($qh3_)) {
                                                                                        print "$data_3_[username], ";
                                                                                    }
                                                                                } else {
                                                                                    print "<i>None</i>";
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    <?php }
                                                                    ?>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <?php
//                                                print "\t<a href=\"javascript:void(0)\" onclick=\"javascript:window.open('task_info.php?task_id=$data[task_id]','Task Info','location=0,directories=no,status=no,scrollbar=yes,menubar=no,resizable=1,width=300,height=150')\">$current_task_name</a>\n\t";
                                            }
                                            print "&nbsp;</td>\n\t<td valign=\"top\" align=\"left\" width=\"8%\" class=\"calendar_cell_right\">$data[start_date]:&nbsp;&nbsp;</td>\n\t";
                                            print "</td>\n\t<td valign=\"top\" align=\"left\" class=\"calendar_cell_right\">";
                                            if ($data['log_message'])
                                                print stripslashes($data['log_message']);
                                            else
                                                print "&nbsp;";
                                            print "</td>\n\t";
                                            print "<td valign=\"bottom\" align=\"right\" width=\"5%\" class=\"calendar_cell_right\">\n\t\t";
                                            $trans_num = $data['trans_num'];

//get the timeformat
                                            $timeFormat = getTimeFormat();

                                            $dateFormatString = ($timeFormat == "12") ? "%m/%d/%Y %h:%i%p" : "%m/%d/%Y %H:%i";

                                            $query_tr = "SELECT DATE_FORMAT(start_time, '$dateFormatString') as formattedStartTime, " .
                                                    "DATE_FORMAT(end_time, '$dateFormatString') as formattedEndTime, " .
                                                    "(unix_timestamp(end_time) - unix_timestamp(start_time)) as time," .
                                                    "log_message, " .
                                                    "$PROJECT_TABLE.title AS projectTitle, " .
                                                    "$PROJECT_TABLE.proj_status AS projectStatus, " .
                                                    "$TASK_TABLE.name AS taskName, " .
                                                    "$TASK_TABLE.status AS taskStatus, " .
                                                    "$CLIENT_TABLE.organisation, " .
                                                    "$USER_TABLE.first_name, " .
                                                    "$USER_TABLE.last_name " .
                                                    "FROM $TIMES_TABLE, $PROJECT_TABLE, $TASK_TABLE, $USER_TABLE, $CLIENT_TABLE " .
                                                    "WHERE $PROJECT_TABLE.proj_id=$TIMES_TABLE.proj_id " .
                                                    "AND $TASK_TABLE.task_id=$TIMES_TABLE.task_id " .
                                                    "AND $TIMES_TABLE.trans_num=$trans_num " .
                                                    "AND $PROJECT_TABLE.client_id = $CLIENT_TABLE.client_id " .
                                                    "AND $USER_TABLE.username = $TIMES_TABLE.uid";
                                            ?>
                                            <div class="row no-margin">
                                                <a href="#" data-toggle="modal" data-target="#myModal_tr_<?php echo $data['trans_num'] ?>"><?php echo $data['diff_time']; ?></a>
                                                <div id="myModal_tr_<?php echo $data['trans_num']; ?>" class="modal fade" role="dialog">
                                                    <div class="modal-dialog modal-default">
                                                        <div class="modal-content text-left">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                                <h4 class="modal-title">Trans info</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <?php
                                                                list($qh_tr, $num_tr) = dbQuery($query_tr);
                                                                if ($num_tr > 0) {
                                                                    $data_tr = dbResult($qh_tr);
                                                                    ?>
                                                                    <div class="row no-margin">
                                                                        <div class="col-sm-12">
                                                                            <span>Project:</span>
                                                                            <span class="project_title"><?php echo stripslashes($data_tr["projectTitle"]); ?></span>
                                                                            &nbsp;<span class="project_status">&lt;<?php echo $data_tr["projectStatus"]; ?>&gt;</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row no-margin">
                                                                        <div class="col-sm-12">
                                                                            <span>Task:</span>
                                                                            <span class="task_title"><?php echo stripslashes($data_tr["taskName"]); ?></span>
                                                                            &nbsp;<span class="project_status">&lt;<?php echo $data_tr["taskStatus"]; ?>&gt;</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row no-margin">
                                                                        <div class="col-sm-12">
                                                                            <span>Clocked On:</span>
                                                                            <?php echo $data_tr["formattedStartTime"]; ?>&nbsp;
                                                                            <span class="label">Clocked Off:</span>
                                                                            <?php echo $data_tr["formattedEndTime"]; ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row no-margin">
                                                                        <div class="col-sm-12">
                                                                            <span>Duration:</span>
                                                                            <?php echo formatSeconds($data_tr["time"]); ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row no-margin">
                                                                        <div class="col-sm-12">
                                                                            <span>Log Message:</span>						
                                                                            <?php echo $data_tr["log_message"]; ?>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <?php
//                                            print "<a href=\"javascript:void(0)\" onclick=\"javascript:window.open(" .
//                                            "'trans_info.php?trans_num=$data[trans_num]'" .
//                                                    ",'Task Event Info','location=0,directories=no" .
//                                                    ",status=no,scrollbar=yes,menubar=no,resizable=1,width=500,height=200')\">&nbsp;&nbsp;$data[diff_time]<a>\n\t";
                                            print "</td>\n</tr>\n";
                                            $total_time += $data["diff"];
                                            $grand_total_time += $data["diff"];
                                        }

                                        if ($total_time) {
                                            $formatted_time = format_seconds($total_time);
                                            print "<tr><td colspan=\"4\" align=\"right\" class=\"calendar_totals_line_weekly_right\">" .
                                                    "Total: <span class=\"calendar_total_value_weekly\">$formatted_time</span></td></tr>";
                                        }
                                        $formatted_time = format_seconds($grand_total_time);
                                    }
                                    ?>
                                    <?php
                                    if ($num > 0) {
                                        ?> 
                                        <tr>
                                            <td colspan="4" align="right">
                                                <?php
                                                if ($mode == "weekly")
                                                    print "Weekly";
                                                else
                                                    print "Monthly";
                                                ?>						
                                                total:
                                                <span><?php echo $formatted_time; ?></span>
                                            </td>
                                        </tr>

                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </body>
</HTML>

