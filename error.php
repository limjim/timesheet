<?php
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");

//continue session
session_start();

//get the logged in user
$loggedInUser = $_SESSION['loggedInUser'];

//load local vars from superglobals
$errormsg = stripslashes($_REQUEST['errormsg']);

//define the command menu
$commandMenu->add(new TextCommand("fa fa-mail-reply-all", "Back", true, "javascript:history.back()"));
?>
<HTML>
    <HEAD>
        <TITLE>Error, <? echo $loggedInUser; ?></TITLE>
        <?php
        include ("header.inc");
        ?>
    </HEAD>
    <BODY class="skin-blue sidebar-mini" >
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <?php
                    include ("error.inc");
                    ?>
                </section>
            </div>
        </div> 
    </BODY>
</HTML>
