<?php
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn()) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//load local vars from superglobals
$month = $_REQUEST['month'];
$day = $_REQUEST['day'];
$year = $_REQUEST['year'];
$client_id = $_REQUEST['client_id'];
$proj_id = $_REQUEST['proj_id'];
$task_id = $_REQUEST['task_id'];
$origin = $_REQUEST["origin"];
$destination = $_REQUEST["destination"];
$clock_on_time_hour = $_REQUEST['clock_on_time_hour'];
$clock_on_time_min = $_REQUEST['clock_on_time_min'];
$clock_off_time_hour = $_REQUEST['clock_off_time_hour'];
$clock_off_time_min = $_REQUEST['clock_off_time_min'];
$clockonoff = $_REQUEST['clockonoff'];

// create the command menu cancel option
$commandMenu->add(new TextCommand("fa fa-mail-reply-all", "Cancel", true, "$destination?client_id=$client_id&proj_id=$proj_id&task_id=$task_id&year=$year&month=$month&day=$day"));
?>
<html>
    <head>
        <title>Clock off - Enter log message</title>
        <?
        include ("header.inc");
        ?>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <div class="row no-margin">
                        <div class="col-md-offset-3 col-md-6">
                            <form  class="form-horizontal" action="action.php" method="post">
                                <input type="hidden" name="origin" value="<?php echo $origin; ?>">
                                <input type="hidden" name="destination" value="<?php echo $destination; ?>">
                                <input type="hidden" name="clock_on_time_hour" value="<?php echo $clock_on_time_hour; ?>">
                                <input type="hidden" name="clock_off_time_hour" value="<?php echo $clock_off_time_hour; ?>">
                                <input type="hidden" name="clock_on_time_min" value="<?php echo $clock_on_time_min; ?>">
                                <input type="hidden" name="clock_off_time_min" value="<?php echo $clock_off_time_min; ?>">
                                <input type="hidden" name="year" value="<?php echo $year ?>">
                                <input type="hidden" name="month" value="<?php echo $month; ?>">
                                <input type="hidden" name="day" value="<?php echo $day; ?>">
                                <input type="hidden" name="client_id" value="<?php echo $client_id; ?>">	
                                <input type="hidden" name="proj_id" value="<?php echo $proj_id; ?>">
                                <input type="hidden" name="task_id" value="<?php echo $task_id; ?>">
                                <input type="hidden" name="clockonoff" value="<?php echo $clockonoff; ?>">
                                <input type="hidden" name="log_message_presented" value="1">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">
                                            Enter Log Message
                                        </h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="row no-margin top10">
                                            Please Enter Log message: (max 255 characters)
                                        </div>
                                        <div class="row no-margin top10">
                                            <textarea class="form-control" name="log_message" cols="60" rows="4"></textarea>
                                        </div>
                                    </div>
                                    <div class="box-footer text-center">
                                        <input type="submit" class="btn btn-info btn-sm" value="Done">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div> 
        </div>
    </body>
</html>
