<?php
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//load local vars from superglobals
$client_id = $_REQUEST['client_id'];

//define the command menu
$commandMenu->add(new TextCommand("fa fa-backward", "Back", true, "javascript:history.back()"));

//build the query
$query = "select client_id, organisation, description, address1, address2," .
        "city, country, postal_code, contact_first_name, contact_last_name," .
        "username, contact_email, phone_number, fax_number, gsm_number, " .
        "http_url " .
        "from $CLIENT_TABLE " .
        "where $CLIENT_TABLE.client_id=$client_id";

//run the query
list($qh, $num) = dbQuery($query);
$data = dbResult($qh);
?>
<html>
    <head>
        <title>Modify client information</title>
<?php include ("header.inc"); ?>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
<?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <div class="row no-margin">
                        <div class="col-md-offset-2 col-md-8 col-xs-12">
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title"> Edit Client: <?php echo $data["organisation"]; ?></h3>
                                </div>
                                <form class="form-horizontal" action="client_action.php" method="post">
                                    <input type="hidden" name="action" value="edit">
                                    <input type="hidden" name="client_id" value="<?php echo $client_id ?>">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="organisation" class="col-sm-3 control-label">Organisation:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="organisation" class="form-control input-sm" value="<?php echo $data["organisation"]; ?>" name="organisation" placeholder="Organisation">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description" class="col-sm-3 control-label">Description:</label>
                                            <div class="col-sm-9">
                                                <textarea name="description" rows="4" class="form-control" id="description"><?php echo trim($data["description"]); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="address1" class="col-sm-3 control-label">Address1:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="address1" class="form-control input-sm" value="<?php echo $data["address1"]; ?>" name="address1" placeholder="Address1">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="address2" class="col-sm-3 control-label">Address2:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="address2" class="form-control input-sm" value="<?php echo $data["city"]; ?>" name="address2" placeholder="Address2">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="city" class="col-sm-3 control-label">City:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="city" class="form-control input-sm" value="<?php echo $data["city"]; ?>" name="city" placeholder="City">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="country" class="col-sm-3 control-label">Country:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="country" class="form-control input-sm" value="<?php echo $data["country"]; ?>" name="country" placeholder="Country">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="postal_code" class="col-sm-3 control-label">Postal Code:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="postal_code" class="form-control input-sm" value="<?php echo $data["postal_code"]; ?>" name="postal_code" placeholder="Postal Code">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="contact_frstname" class="col-sm-3 control-label">Contact Firstname:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="contact_frstname" class="form-control input-sm" value="<?php echo $data["contact_first_name"]; ?>" name="contact_first_name" placeholder="Contact Firstname">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="contact_lastname" class="col-sm-3 control-label">Contact Lastname:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="contact_lastname" class="form-control input-sm" value="<?php echo $data["contact_last_name"]; ?>" name="contact_last_name" placeholder="Contact Lastname">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="username" class="col-sm-3 control-label">Username:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="username" class="form-control input-sm" value="<?php echo $data["username"]; ?>" name="client_username" placeholder="Username">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="contact_email" class="col-sm-3 control-label">Contact email:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="contact_email" class="form-control input-sm" value="<?php echo $data["contact_email"]; ?>" name="contact_email" placeholder="Contact email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone_number" class="col-sm-3 control-label">Phone Number:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="phone_number" class="form-control input-sm" value="<?php echo $data["phone_number"]; ?>" name="phone_number" placeholder="Phone Number">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="fax_number" class="col-sm-3 control-label">Fax Number:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="fax_number" class="form-control input-sm" value="<?php echo $data["fax_number"]; ?>" name="fax_number" placeholder="Fax Number">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gsm_number" class="col-sm-3 control-label">Mobile Number:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="gsm_number" class="form-control input-sm" value="<?php echo $data["gsm_number"]; ?>" name="gsm_number" placeholder="Mobile Number">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="http_url" class="col-sm-3 control-label">Website:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="http_url" class="form-control input-sm" value="<?php echo $data["http_url"]; ?>" name="http_url" placeholder="Mobile Number">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <input type="submit" class="btn btn-info pull-right" name="edit" value="Save">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </body>
</HTML>
