<?php
//$Header: /cvsroot/tsheet/timesheet.php/proj_edit.php,v 1.7 2005/05/16 01:39:57 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//load local vars from superglobals
$proj_id = $_REQUEST['proj_id'];

//define the command menu
$commandMenu->add(new TextCommand("fa fa-backward", "Back", true, "javascript:history.back()"));

$dbh = dbConnect();
list($qh, $num) = dbQuery("select proj_id, title, client_id, description, DATE_FORMAT(start_date, '%m') as start_month, " .
        "date_format(start_date, '%d') as start_day, date_format(start_date, '%Y') as start_year, " .
        "DATE_FORMAT(deadline, '%m') as end_month, date_format(deadline, '%d') as end_day, date_format(deadline, '%Y') as end_year, " .
        "http_link, proj_status, proj_leader from $PROJECT_TABLE where proj_id = $proj_id order by proj_id");
$data = dbResult($qh);

list($qh, $num) = dbQuery("SELECT username from $ASSIGNMENTS_TABLE where proj_id = $proj_id");
$selected_array = array();
$i = 0;
while ($datanext = dbResult($qh)) {
    $selected_array[$i] = $datanext["username"];
    $i++;
}
?> 

<html>
    <head>
        <title>Edit Project</title>
        <?php include ("header.inc"); ?>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 col-xs-12">
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit project: <?php echo stripslashes($data["title"]); ?></h3>
                                </div>
                                <form class="form-horizontal" action="proj_action.php" method="post">
                                    <input type="hidden" name="action" value="edit">
                                    <input type="hidden" name="proj_id" value="<?php echo $data["proj_id"]; ?>">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="project_title" class="col-sm-3 control-label">Project Title:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="project_title" value="<?php echo stripslashes($data["title"]); ?>" class="form-control input-sm" name="title" placeholder="Project title">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="project_title" class="col-sm-3 control-label">Client:</label>
                                            <div class="col-sm-9">
                                                <?php client_select_list($data["client_id"], 0, false, false, false, true, "", false); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Description:</label>
                                            <div class="col-sm-9">
                                                <textarea name="description" rows="4" cols="40" wrap="virtual" class="form-control"><?php
                                                    $data["description"] = stripslashes($data["description"]);
                                                    echo $data["description"];
                                                    ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Start Date:</label>
                                            <div class="col-sm-9">
                                                <div class="row no-margin">
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        day_button("start_day", $data["start_day"]);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        month_button("start_month", $data["start_month"]);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        year_button("start_year", $data["start_year"]);
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Deadline:</label>
                                            <div class="col-sm-9">
                                                <div class="row no-margin">
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        day_button("end_day", $data["end_day"]);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        month_button("end_month", $data["end_month"]);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        year_button("end_year", $data["end_year"]);
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Status:</label>
                                            <div class="col-sm-9">
                                                <?php proj_status_list("proj_status", $data["proj_status"]); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="project_url" class="col-sm-3 control-label">URL:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="project_url" name="url" value="<?php echo $data["http_link"]; ?>" class="form-control input-sm"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Assignments:</label>
                                            <div class="col-sm-9">
                                                <?php multi_user_select_list("assigned[]", $selected_array); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Project Leader:</label>
                                            <div class="col-sm-9">
                                                <?php single_user_select_list("project_leader", $data["proj_leader"]); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <input type="submit" name="edit" class="btn btn-info pull-right" value="Save">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </body>
</html>
