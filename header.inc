<?php
require("table_names.inc");

//Get the result set for the config set 1
list($qhq, $numq) = dbQuery("select headerhtml from $CONFIG_TABLE where config_set_id = '1'");
$configdata = dbResult($qhq);
parse_and_echo(stripslashes($configdata["headerhtml"]));
?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link href="css/libs/bootstrap.min.css" rel="stylesheet" media="screen" type="text/css">
<link href="css/libs/AdminLTE.min.css" media="screen" rel="stylesheet" type="text/css">
<link href="css/libs/_all-skins.min.css" rel="stylesheet" media="screen" type="text/css">
<link href="css/libs/dataTables.bootstrap.css" rel="stylesheet" media="screen" type="text/css">
<link href="css/libs/fullcalendar.min.css" rel="stylesheet" type="text/css">
<link href="css/libs/fullcalendar.print.css" rel="stylesheet" media="print" type="text/css">


<script type="text/javascript" src="js/jQuery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/fastclick.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="js/app.min.js"></script>
<script type="text/javascript" src="js/demo.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script type="text/javascript" src="js/fullcalendar.min.js"></script>



