<style>
    .scrollme {
        overflow: auto;
        border: 1px solid #F0F0F0;
        height: 140px;
        box-shadow: 0 0 1px #999999;
    }
    .scoll-container{
        padding: 5px;
        top: 0;
    }
</style>
<?php
if (!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
//$Header: /cvsroot/tsheet/timesheet.php/proj_maint.php,v 1.10 2005/05/17 03:38:37 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//define the command menu
include("timesheet_menu.inc");

//set up query
$query = "select distinct $PROJECT_TABLE.title, $PROJECT_TABLE.proj_id, $PROJECT_TABLE.client_id, " .
        "$CLIENT_TABLE.organisation, $PROJECT_TABLE.description, " .
        "DATE_FORMAT(start_date, '%M %d, %Y') as start_date, " .
        "DATE_FORMAT(deadline, '%M %d, %Y') as deadline, " .
        "$PROJECT_TABLE.proj_status, http_link, proj_leader " .
        "FROM $PROJECT_TABLE, $CLIENT_TABLE, $USER_TABLE " .
        "WHERE ";
if ($client_id != 0)
    $query .= "$PROJECT_TABLE.client_id = $client_id AND ";

$query .= "$PROJECT_TABLE.proj_id > 0 AND $CLIENT_TABLE.client_id = $PROJECT_TABLE.client_id " .
        "ORDER BY $PROJECT_TABLE.title";
?>
<html>
    <head>
        <title>Projects</title>
        <?php
        include ("header.inc");
        ?>
        <script language="Javascript">

            function delete_project(clientId, projectId) {
                if (confirm('Deleting a project will also delete all tasks and assignments associated ' +
                        'with that project. If any of these tasks have timesheet entries ' +
                        'they will become invalid and may cause errors. This action is not recommended. ' +
                        'Are you sure you want to delete this project?'))
                    location.href = 'proj_action.php?client_id=' + clientId + '&proj_id=' + projectId + '&action=delete';
            }

        </script>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?> 
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
                        <div class="box">
                            <div class="box-body">
                                <div class="row no-margin">
                                    <div class="col-md-offset-5 col-md-5 col-sm-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Client</label>
                                            <div class="col-sm-10">
                                                <?php client_select_list($client_id, 0, false, false, true, false, "submit();", false); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <a class="btn btn-info btn-sm" href="proj_add.php?client_id=<? echo $client_id; ?>"><i class="fa fa-plus"></i>  Add new project</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Projects</h3>
                        </div>
                        <div class="box-body">
                            <?php
                            list($qh, $num) = dbQuery($query);
                            if ($num == 0) {
                                if ($client_id != 0) {
                                    ?>
                                    <div class="callout callout-info">
                                        <h4>Notice</h4>
                                        <p>There are no projects for this client.</p>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="callout callout-info">
                                        <h4>Notice</h4>
                                        <p>There are no projects.</p>
                                    </div>
                                    <?php
                                }
                            } else {
                                //iterate through results
                                for ($j = 0; $j < $num; $j++) {
                                    //get the current record
                                    $data = dbResult($qh);

                                    //strip slashes
                                    $data["title"] = stripslashes($data["title"]);
                                    $data["organisation"] = stripslashes($data["organisation"]);
                                    $data["description"] = stripslashes($data["description"]);

                                    list($billqh, $bill_num) = dbquery("select sum(unix_timestamp(end_time) - unix_timestamp(start_time)) as total_time, " .
                                            "sum(bill_rate * ((unix_timestamp(end_time) - unix_timestamp(start_time))/(60*60))) as billed " .
                                            "from $TIMES_TABLE, $USER_TABLE " .
                                            "where end_time > 0 AND $TIMES_TABLE.proj_id = $data[proj_id] AND $USER_TABLE.username = $TIMES_TABLE.uid ");
                                    $bill_data = dbResult($billqh);
                                    ?>
                                    <section>
                                        <div class="row margin">
                                            <div class="col-xs-12">
                                                <div class="box box-info">
                                                    <div class="box-header">
                                                        <div class="col-md-6">
                                                            <div class="pull-left">
                                                                <h3 class="box-title">
                                                                    <?php
                                                                    if ($data["http_link"] != "") {
                                                                        print "<a href=\"$data[http_link]\">$data[title]</a>";
                                                                    } else {
                                                                        print "$data[title]";
                                                                    }
                                                                    ?>
                                                                </h3>
                                                                <small>
                                                                    <?php
                                                                    print "$data[proj_status]"
                                                                    ?>
                                                                </small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="pull-right box-tools">
                                                                <a class="btn btn-xs btn-warning" href="proj_edit.php?client_id=<?php echo $client_id; ?>&proj_id=<?php echo $data["proj_id"]; ?>"><i class="fa fa-edit"></i>  Edit</a>
                                                                <a class="btn btn-xs btn-danger" href="javascript:delete_project(<?php echo $client_id; ?>,<?php echo $data["proj_id"]; ?>);"><i class="fa fa-remove"></i>  Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="row margin">
                                                            <div class="col-md-offset-6 col-md-6 col-xs-12">
                                                                <?php if (isset($data["start_date"]) && $data["start_date"] != '' && $data["deadline"] != '') { ?>

                                                                    <div class="pull-left">
                                                                        <span class="text-light-blue">Start date: </span><?php echo date("d-m-Y", strtotime("$data[start_date]")); ?>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <span class="text-red">Deadline:</span> <?php echo date("d-m-Y", strtotime("$data[start_date]")); ?>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="row margin">
                                                            <div class="col-md-6 col-xs-12 text-left">
                                                                <dl class="dl-horizontal">
                                                                    <dt>Description:</dt>
                                                                    <dd><?php echo $data["description"]; ?></dd>   
                                                                    <dt>Total time:</dt>
                                                                    <dd><?php echo (isset($bill_data["total_time"]) ? formatSeconds($bill_data["total_time"]) : "0h 0m"); ?></dd>
                                                                    <dt>Total bill:</dt>
                                                                    <dd>$<?php echo (isset($bill_data["billed"]) ? $bill_data["billed"] : "0.00"); ?></dd>
                                                                    <dt>Project Leader:</dt>
                                                                    <dd><?php print "$data[proj_leader]"; ?></dd>
                                                                    <?php
                                                                    list($qh2, $num_workers) = dbQuery("select distinct username from $ASSIGNMENTS_TABLE where proj_id = $data[proj_id]");
                                                                    if ($num_workers == 0) {
                                                                        print "<dt>Nobody assigned to this project</dt>";
                                                                    } else {
                                                                        $workers = '';
                                                                        print "<dt>Assigned Users: </dt> ";
                                                                        for ($k = 0; $k < $num_workers; $k++) {
                                                                            $worker = dbResult($qh2);
                                                                            $workers .= "$worker[username], ";
                                                                        }
                                                                        ?>
                                                                        <dd>
                                                                            <?php
                                                                            print $workers;
                                                                        }
                                                                        ?>
                                                                    </dd>
                                                                </dl>
                                                            </div>
                                                            <div class="col-md-6 col-xs-12">
                                                                <div class="row margin">
                                                                    <div class="box box-success">
                                                                        <div class="box-header ui-sortable-handle">
                                                                            <i class="fa fa-tasks"></i>
                                                                            <h3 class="box-title"><a href="task_maint.php?proj_id=<?php echo $data["proj_id"]; ?>">Tasks:</a></h3>
                                                                        </div>

                                                                        <div class="scrollme" style="position: relative;">
                                                                            <div class="scoll-container">
                                                                                <?php
                                                                                //get tasks
                                                                                list($qh3, $num_tasks) = dbQuery("select name, task_id FROM $TASK_TABLE WHERE proj_id=$data[proj_id]");

                                                                                //are there any tasks?
                                                                                if ($num_tasks > 0) {

                                                                                    while ($task_data = dbResult($qh3)) {

                                                                                        $taskName = str_replace(" ", "&nbsp;", $task_data["name"]);
                                                                                        $task_id = $task_data['task_id'];
                                                                                        $query_task = "select distinct task_id, name, description,status, " .
                                                                                                "DATE_FORMAT(assigned, '%M %d, %Y') as assigned," .
                                                                                                "DATE_FORMAT(started, '%M %d, %Y') as started," .
                                                                                                "DATE_FORMAT(suspended, '%M %d, %Y') as suspended," .
                                                                                                "DATE_FORMAT(completed, '%M %d, %Y') as completed " .
                                                                                                "from $TASK_TABLE " .
                                                                                                "where $TASK_TABLE.task_id=$task_id " .
                                                                                                "order by $TASK_TABLE.task_id";

//get the proj_id for this task
                                                                                        if (!isset($proj_id)) {
                                                                                            list($qh_m, $num_m) = $proj_id = dbQuery("SELECT proj_id FROM $TASK_TABLE where task_id='$task_id'");
                                                                                            $results_m = dbResult($qh);
                                                                                            $proj_id = $results["proj_id"];
                                                                                        }

                                                                                        $query_project = "select distinct title, description," .
                                                                                                "DATE_FORMAT(start_date, '%M %d, %Y') as start_date," .
                                                                                                "DATE_FORMAT(deadline, '%M %d, %Y') as deadline," .
                                                                                                "proj_status, proj_leader " .
                                                                                                "from $PROJECT_TABLE " .
                                                                                                "where $PROJECT_TABLE.proj_id=$proj_id";
                                                                                        ?>
                                                                                        <div class="row no-margin">
                                                                                            <a href="#" data-toggle="modal" data-target="#myModal_<?php echo $task_data['task_id'] ?>"><?php echo $taskName; ?></a>
                                                                                            <div id="myModal_<?php echo $task_data['task_id']; ?>" class="modal fade" role="dialog">
                                                                                                <div class="modal-dialog modal-default">


                                                                                                    <div class="modal-content">
                                                                                                        <div class="modal-header">
                                                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                                                                            <h4 class="modal-title">Task info</h4>
                                                                                                        </div>
                                                                                                        <div class="modal-body">
                                                                                                            <?php
                                                                                                            list($qh_m, $num_m) = dbQuery($query_task);
                                                                                                            if ($num_m > 0) {
                                                                                                                $data_task_ = dbResult($qh_m);
                                                                                                                ?>
                                                                                                                <div class="row no-margin">
                                                                                                                    <div class="col-sm-10 col-xs-12">
                                                                                                                        <h4 class="box-title text-blue"><?php echo stripslashes($data_task_["name"]); ?></h4>
                                                                                                                    </div>

                                                                                                                    <div class="col-sm-2 col-xs-12" style="padding: 12px 0px 0px 0px;">
                                                                                                                        <small>&lt;<?php echo $data_task_["status"]; ?>&gt;</small>
                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                <div class="row no-margin">
                                                                                                                    <div class="col-sm-12 col-xs-12">
                                                                                                                        <?php echo stripslashes($data_task_["description"]); ?>

                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="row no-margin">
                                                                                                                    <div class="col-sm-12 col-xs-12">
                                                                                                                        <label>Assigned persons:</label><br>
                                                                                                                        <?php
                                                                                                                        //get assigned users
                                                                                                                        list($qh3_, $num_3_) = dbQuery("select username, task_id from $TASK_ASSIGNMENTS_TABLE where task_id=$task_id");
                                                                                                                        if ($num_3_ > 0) {
                                                                                                                            while ($data_3_ = dbResult($qh3_)) {
                                                                                                                                print "$data_3_[username], ";
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            print "<i>None</i>";
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <?php }
                                                                                                            ?>
                                                                                                        </div>
                                                                                                        <div class="modal-footer">
                                                                                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                        <?php
//                                                                                        print "<a href=\"javascript:void(0)\" onclick=window.open(\"task_info.php?proj_id=$data[proj_id]&task_id=$task_data[task_id]\",\"TaskInfo\",\"location=0,directories=no,status=no,menubar=no,resizable=1,width=550,height=220\")>$taskName</a><br>";
                                                                                    }
                                                                                } else
                                                                                    print "None.";
                                                                                ?>	
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                    </section>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </body>
</HTML>

