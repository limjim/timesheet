<?php
//$Header: /cvsroot/tsheet/timesheet.php/proj_add.php,v 1.9 2005/05/16 01:39:57 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//define the command menu
$commandMenu->add(new TextCommand("fa fa-backward","Back", true, "javascript:history.back()"));

//load client id from superglobals
$client_id = isset($_REQUEST['client_id']) ? $_REQUEST['client_id']: 1;

?>
<html>
    <head>
        <title>Add New Project</title>
        <?php include ("header.inc"); ?>
    </head>

    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 col-xs-12">
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add new project</h3>
                                </div>
                                <form class="form-horizontal" action="proj_action.php" method="post">
                                    <input type="hidden" name="action" value="add">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="project_title" class="col-sm-3 control-label">Project Title:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="project_title" class="form-control input-sm" name="title" placeholder="Project title">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="project_title" class="col-sm-3 control-label">Client:</label>
                                            <div class="col-sm-9">
                                                <?php client_select_list($client_id, 0, false, false, false, true, "", false); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Description:</label>
                                            <div class="col-sm-9">
                                                <textarea name="description" rows="4" cols="40" wrap="virtual" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Start Date:</label>
                                            <div class="col-sm-9">
                                                <div class="row no-margin">
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        day_button("start_day");
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        month_button("start_month");
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        year_button("start_year");
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Deadline:</label>
                                            <div class="col-sm-9">
                                                <div class="row no-margin">
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        day_button("end_day");
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        month_button("end_month");
                                                        ?>
                                                    </div>
                                                    <div class="col-md-3 no-margin">
                                                        <?php
                                                        year_button("end_year");
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Status:</label>
                                            <div class="col-sm-9">
                                                <?php proj_status_list("proj_status", "Started"); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="project_url" class="col-sm-3 control-label">URL:</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="project_url" name="url" class="form-control input-sm"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Assignments:</label>
                                            <div class="col-sm-9">
                                                <?php multi_user_select_list("assigned[]"); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Project Leader:</label>
                                            <div class="col-sm-9">
                                                <?php single_user_select_list("project_leader"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <input type="submit" name="add" class="btn btn-info pull-right" value="Save">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </body>
</html>
