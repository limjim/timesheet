<?php
// $Header: /cvsroot/tsheet/timesheet.php/task_add.php,v 1.6 2004/07/02 14:15:56 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn() || !$authenticationManager->hasClearance(CLEARANCE_ADMINISTRATOR)) {
Header("Location: login.php?redirect=$_SERVER[PHP_SELF]&clearanceRequired=Administrator");
exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//load local vars from superglobals
$proj_id = $_REQUEST['proj_id'];

//define the command menu
$commandMenu->add(new TextCommand("fa fa-backward","Back", true, "javascript:history.back()"));

?>
<html>
    <head>
        <title>Add New Task</title>
        <?php include ("header.inc"); ?>
    </head>

    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include ("header_body.inc"); ?>
            <?php include ("left_menu.inc"); ?>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 col-xs-12">
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add new task</h3>
                                </div>
                                <form class="form-horizontal" action="task_action.php" method="post">
                                    <input type="hidden" name="action" value="add">
                                    <input type="hidden" name="proj_id" value="<?php echo $proj_id ?>">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="task_name" class="col-sm-3 control-label">Task Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control input-sm" id="task_name" name="name" placeholder="Task name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description_task" class="col-sm-3 control-label">Description</label>
                                            <div class="col-sm-9">
                                                <textarea type="textarea" class="form-control" rows="3" id="description_task" name="description" placeholder="Description"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Status:</label>
                                            <div class="col-sm-9">
                                                <?php proj_status_list("task_status", "Started"); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Assignments</label>
                                            <div class="col-sm-9">
                                                <?php multi_user_select_list("assigned[]"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <input type="submit" class="btn btn-info pull-right" value="Save">
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </body>
</html>