<?php
//$Header: /cvsroot/tsheet/timesheet.php/stopwatch.php,v 1.5 2005/05/17 03:38:37 vexil Exp $
// Authenticate
require("class.AuthenticationManager.php");
require("class.CommandMenu.php");
if (!$authenticationManager->isLoggedIn()) {
    Header("Location: login.php?redirect=$_SERVER[PHP_SELF]");
    exit;
}

// Connect to database.
$dbh = dbConnect();
$contextUser = strtolower($_SESSION['contextUser']);

//load local vars from superglobals
$proj_id = isset($_REQUEST["proj_id"]) ? $_REQUEST["proj_id"] : 0;
$task_id = isset($_REQUEST["task_id"]) ? $_REQUEST["task_id"] : 0;
$client_id = isset($_REQUEST["client_id"]) ? $_REQUEST["client_id"] : 0;
$destination = $_REQUEST["destination"];

//check that the client id is valid
if ($client_id == 0)
    $client_id = getFirstClient();

//check that project id is valid
if ($proj_id == 0)
    $task_id = 0;
?>
<html>
    <head>
        <title>	Timesheet.php Stopwatch, <? echo $contextUser; ?></title>
        <?php
        include ("header.inc");
        include("client_proj_task_javascript.inc");
        ?>

        <script language="javascript">

            function doClockonoff(clockon) {
                document.mainForm.clockonoff.value = clockon;
                onSubmit();
            }

            function resizePopupWindow() {
                //now resize the window
                var outerTable = document.getElementById('outer_table');
                var newWidth = outerTable.offsetWidth + window.outerWidth - window.innerWidth;
                var newHeight = outerTable.offsetHeight + window.outerHeight - window.innerHeight;
                window.resizeTo(newWidth, newHeight);
            }

        </script>
    </head>
    <body style="margin: 0;"  onload="doOnLoad();">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            Clock On / Off Now
                        </h3>
                    </div>
                    <form class="form-horizontal" action="action.php" method="post" name="mainForm" id="theForm">					
                        <input type="hidden" name="year" value="<?php echo date('Y'); ?>">
                        <input type="hidden" name="month" value="<?php echo date('m'); ?>">	
                        <input type="hidden" name="day" value="<?php echo date('j'); ?>">
                        <input type="hidden" id="client_id" name="client_id" value="<?php echo $client_id; ?>">						
                        <input type="hidden" id="proj_id" name="proj_id" value="<?php echo $proj_id; ?>">
                        <input type="hidden" id="task_id" name="task_id" value="<?php echo $task_id; ?>">
                        <input type="hidden" name="clockonoff" value="">
                        <input type="hidden" name="fromPopupWindow" value="true">																			
                        <input type="hidden" name="destination" value="<?php echo $destination; ?>">
                        <input type="hidden" name="origin" value="<?php echo $_SERVER["PHP_SELF"]; ?>">
                        <div class="box-body">
                            <table width="100%" border="0" class="table_body">			
                                <tr>
                                    <td>
                                        <div class="hidden" id="client_">
                                            <div class="row form-group no-margin">
                                                <label class="col-md-3 control-label">Client:</label>
                                                <div class="col-md-9">
                                                    <select id="clientSelect" class="form-control input-sm" name="clientSelect" onChange="onChangeClientSelect();" />
                                                </div>
                                            </div>
                                        </div>
                                    </td>									
                                </tr>																									
                                <tr>
                                    <td>
                                        <div class="row form-group no-margin">
                                            <label class="col-md-3 control-label">Project:</label>
                                            <div class="col-md-9">
                                                <select id="projectSelect" class="form-control input-sm" name="projectSelect" onChange="onChangeProjectSelect();"/>
                                            </div>
                                        </div>
                                    </td>									
                                </tr>																		
                                <tr>
                                    <td>
                                        <div class="row form-group no-margin">
                                            <label class="col-md-3 control-label">Task:</label>
                                            <div class="col-md-9">
                                                <select id="taskSelect" class="form-control input-sm"  name="taskSelect" onChange="onChangeTaskSelect();"/>
                                            </div>
                                        </div>
                                    </td>									
                                </tr>																										
                            </table>
                        </div>
                        <div class="box-footer text-center">
                            <div class="btn-group">
                                <a class="btn btn-sm btn-primary" href="javascript:doClockonoff('clockonnow')"><i class="fa fa-clock-o"></i>   Clock on now</a>
                                <a class="btn btn-sm btn-danger" href="javascript:doClockonoff('clockoffnow')"><i class="fa fa-clock-o"></i>   Clock off now</a>
                            </div>
                        </div>
                    </form>	
                </div>
            </div>
        </div>
    </body>
</html>
